from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from common import urls as url_common
from clientes import urls as url_client
from proyectos import urls as url_proyectos




urlpatterns = [
	path('admin/', admin.site.urls),
	path('', include(url_common)),
	path('', include(url_client)),
	path('', include(url_proyectos)),
	
 	 
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


admin.site.site_header = "Seebaan Admin"
admin.site.site_title = "Seebaan "
admin.site.index_title = "Bienvenido a Seebaan"