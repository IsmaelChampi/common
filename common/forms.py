from django import forms
from common.models import Perfil
from django.utils.translation import ugettext as _

class PerfilForm(forms.ModelForm):
    
    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('First Name(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese un nombre válido')},
        label=_('First Name(s)'),
        required=True,
    )

    last_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Last Name(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese un apellido válido')},
        label=_('Last Name(s)*'),
        required=True,
    )

    phone = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Phone'), 'class': "form-control"}),
        error_messages={'required': _(u'Phone')},
        label=_('Phone'),
        required=True,
    )

    load = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Load'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Load')},
        label=_('Load'),
        required=True,
    )

    birth_date = forms.DateField(
        label=_('Birthdate'),
    )



    class Meta:
        model = Perfil
        fields = ('first_name', 'last_name', 'phone', 'load', 'birth_date', )

    


