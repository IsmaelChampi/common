from django.shortcuts import render, redirect
from common.forms import PerfilForm
from common.models import Perfil
from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout

# Create login

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect('/')
        else:
            return render(request, 'login.html', {'error': 'Invalid username and password'})
    return render(request, 'login.html')


def logout_view(request):
    logout(request)
    return redirect('/login')
    
# Create home
@login_required(login_url='/login')
def home(request):
    return render(request, 'home.html')

@login_required(login_url='/login')
def config_user(request):
    #instancia
    instancia = Perfil.objects.get(pk=request.user.pk)
    # Creamos formulario con los datos de la instancia
    form = PerfilForm(request.POST, instance=instancia)  #objeto
    #comprobamos formulario
    if request.method == 'POST':
        # Actualizamos el formulario con los datos recibidos
        form = PerfilForm(request.POST, instance=instancia)
        # Si el formulario es válido
        if form.is_valid():
            form.save()
            instancia = form.save(commit=False)
            instancia.save()
            return redirect('home')
    else:
        form = PerfilForm() #nuevo objeto
       

    return render(
        request=request,
        template_name='config_user.html',
        context={
            
            'form': form,
            'instancia': instancia,
        })

def PasswordResetView(request):
    return render('password_reset_form.html')

def PasswordResetConfirmView(request):
    return render('')

@login_required(login_url='/login')      
def detail_user(request):

    perfiles = Perfil.objects.all()

    return render(
        request=request,
        template_name='detail.html',
        context={
             'perfiles': perfiles,
          
        })

