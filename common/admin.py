from django.contrib import admin

from common.models import Perfil
# Register your models here.


@admin.register(Perfil)
class PerfilAdmin(admin.ModelAdmin):
    list_display = ('username', 'first_name')
    list_per_page = 30
