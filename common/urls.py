from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.urls import path
from django.contrib.auth import views as auth_views

# View
from common import views 


urlpatterns = [

    # Posts

    path('password_reset/', auth_views.PasswordResetView.as_view(success_url='done/'), name="password_reset"),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name="password_reset_done"),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view( success_url='reset/done/'), name="password_reset_confirm"),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name="password_reset_complete"),
    path('config', views.config_user, name='config_user'),
    path('detail', views.detail_user, name='detail_user'),
    path('', views.home, name='home'), 
    path('login', views.login_view, name='login'),
    path('logout',  views.logout_view, name='logout'),
      
]