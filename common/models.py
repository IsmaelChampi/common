#Models de usuarios
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext as _
# Create your models here.


class Perfil(AbstractUser):
    """ Modelo de perfil de usuario """
    USERNAME_FIELD = 'username'
    phone = models.CharField(max_length=15, blank=True)
    load = models.TextField(max_length=30, blank=True)
    birth_date = models.DateField(blank=True, null=True)
   
