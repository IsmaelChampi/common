from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext as _
from common.models import Perfil
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
# Create your models here.



class Categoria(models.Model):
    nombre = models.CharField(max_length=100)
    def __str__(self):
        return '%s' % self.nombre

class Tag(models.Model):
    nombre = models.CharField(max_length=30)
    color = models.CharField(max_length=30)
    def __str__(self):
        return '%s' % self.nombre

from clientes.models import Client


class Proyect(models.Model):
    nombre = models.TextField(max_length=30)
    categoria = models.ForeignKey('Categoria', on_delete=models.CASCADE,)
    detalles = models.TextField(max_length=1000, blank=True)
    usuario = models.ForeignKey(Perfil, on_delete=models.CASCADE, verbose_name=_('Usuario'))
    client = models.ForeignKey(Client, on_delete=models.CASCADE,)
    #monto = models.FloatField(null=True, blank=True, default = 0)
    #extras = models.FloatField(null=True, blank=True, default = 0)
    total = models.FloatField(null=True, blank=True, default = 0)
    adelanto = models.FloatField(null=True, blank=True, default = 0)
    restante = models.FloatField(null=True, blank=True, default = 0)
    tag = models.ManyToManyField(Tag, blank=True,)
    estado_option = (
        (1, 'Pendiente'),
        (2, 'Proceso'),
        (3, 'Completado'),
        (4, 'Cancelado'),
        
        )
    estado = models.IntegerField(verbose_name=_('estado'), choices=estado_option, blank=True, null=True)
    avance = models.IntegerField(null=True, blank=True)
    fecha =  models.DateTimeField(verbose_name=_('Fecha'), auto_now_add=True)


    hosting_options = (
        (1, 'Interno'),
        (2, 'Externo'),
    )

    hosting= models.IntegerField(verbose_name=_('Hospedaje'), choices=hosting_options, blank=True)
    datos_hosting = models.CharField(max_length=500, blank=True)

    dominio_options = (
        (1, 'Interno'),
        (2, 'Externo'),
    )

    dominio = models.IntegerField(verbose_name=_('Dominio'), choices=dominio_options, blank=True)
    datos_dominio = models.CharField(max_length=500, blank=True)


    size_options = (
        (1, '5 GB'),
        (2, '25 GB'),
        (3, '50 GB'),
        (4, '75 GB'),
    )

    size_hosting = models.IntegerField(verbose_name=_('Tamaño hospedaje'), choices=size_options, blank=True)






class Discusiones(models.Model):
    asunto = models.CharField(max_length=255)
    descripcion = models.TextField(max_length=1000)
    proyecto = models.ForeignKey('Proyect', on_delete=models.CASCADE,)
    usuario = models.ForeignKey(Perfil, on_delete=models.CASCADE, verbose_name=_('Usuario'))
    fecha =  models.DateTimeField(verbose_name=_('Fecha'), auto_now_add=True)

class Tareas(models.Model):
    titulo = models.CharField(max_length=200)
    detalles = models.TextField(max_length=2000)
    fechav = models.DateField(blank=True, null=True)

    estado_option = (
        (1, 'Pendiente'),
        (2, 'Proceso'),
        (3, 'Completado'),
        (4, 'Cancelado'),
        (5, 'Cancelado Definitivo (No contactar)'),
        
        )
    estado = models.IntegerField(verbose_name=_('estado'), choices=estado_option, blank=True, null=True)
    usuario = models.ManyToManyField(Perfil, blank=True, related_name='Usuarios')
    proyecto = models.ForeignKey('Proyect', on_delete=models.CASCADE,)
    prioridad_option = (
        (1, 'Alta'),
        (2, 'Media'),
        (3, 'Baja'),
        
        )
    prioridad = models.IntegerField(verbose_name=_('prioridad'), choices=prioridad_option, blank=True, null=True)
    orden = models.IntegerField(verbose_name=('orden'), blank=True, null=True)
    owner = models.ForeignKey(Perfil, on_delete=models.CASCADE, verbose_name=_('Usuario'), related_name='Creador')
    fechac = models.DateTimeField(verbose_name=_('Fecha'), auto_now_add=True)


class ComentariosTarea(models.Model):
    descripcion = models.TextField(max_length=1000)
    usuario = models.ForeignKey(Perfil, on_delete=models.CASCADE, verbose_name=_('Usuario'))
    tarea = models.ForeignKey('Tareas', on_delete=models.CASCADE,)
    fecha = models.DateTimeField(verbose_name=_('Fecha'), auto_now_add=True)

class ImageTarea(models.Model):
    file = models.FileField(upload_to = 'photos/', default = 'photos')
    tarea = models.ForeignKey('Tareas', on_delete=models.CASCADE,)

class ServiciosExtras(models.Model):
    titulo = models.TextField(max_length=250)
    descripcion = models.TextField(max_length=1000)
    fecha =  models.DateTimeField(verbose_name=_('Fecha'), auto_now_add=True)
    costo = models.IntegerField(verbose_name=('costo'), blank=True, null=True)
    proyecto = models.ForeignKey('Proyect', on_delete=models.CASCADE,)

    tipo_option = (
        (1, 'Servicio Inicial'),
        (2, 'Servicio Extra'),
       
        
        )
    tipo = models.IntegerField(verbose_name=_('tipo'), choices=tipo_option, blank=True, null=True)


class PagosProyecto(models.Model):
    fecha =  models.DateTimeField(verbose_name=_('Fecha'), auto_now_add=True)
    monto = models.IntegerField(verbose_name=('monto'), blank=True, null=True, default=0)
    comentario = models.TextField(max_length=500)
    estatus_option = (
        (1, 'pendiente'),
        (2, 'confirmado'),
        (3, 'cancelado'),
        )
    estatus = models.IntegerField(verbose_name=_('estatus'), choices=estatus_option, blank=True, null=True)
    proyecto = models.ForeignKey('Proyect', on_delete=models.CASCADE,)


class ProyectoPublicidad(models.Model):

    estadocam_option = (
        (1, 'Programada'),
        (2, 'Activa'),
        (3, 'Terminada'),
        (4, 'Cancelada'),
        )
    estado = models.IntegerField(verbose_name=_('estado'), choices=estadocam_option, blank=True, null=True)
    nombre = models.TextField(max_length=30)
    empresa = models.TextField(max_length=30)
    idpublicidad = models.TextField(max_length=30)
    promocion = models.TextField(max_length=30)
    total = models.FloatField(null=True, blank=True, default = 0)
    adelanto = models.FloatField(null=True, blank=True, default = 0)
    restante = models.FloatField(null=True, blank=True, default = 0)
    usuario = models.ForeignKey(Perfil, on_delete=models.CASCADE, verbose_name=_('Usuario'))
    fecha =  models.DateTimeField(verbose_name=_('Fecha'), auto_now_add=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE,)
    
class HistorialCampana(models.Model):
    fechaini = models.DateField(blank=True, null=True)
    fechater = models.DateField(blank=True, null=True)
    inversion = models.IntegerField(verbose_name=('inversion'), blank=True, null=True)
    comision = models.IntegerField(verbose_name=('comision'), blank=True, null=True)
    comentario = models.TextField(max_length=30)
    proyecto = models.ForeignKey('ProyectoPublicidad', on_delete=models.CASCADE,)



# agregamos el parámetro sender con la clase PagosProyecto
# para indicarle a donde tiene que escuchar.
@receiver(post_save, sender=ServiciosExtras)
def post_save_servicio(sender, instance, created, **kwargs):

    # Verifico que se crea un pagoproyecto
    if created:
        #filtarmos por pago y sacamos el id de proyecto
        servicios = ServiciosExtras.objects.filter(proyecto_id=instance.proyecto.id)
        #iteramos para poder sacar la suma total
    
        totalservicios = 0 

        for servi  in   servicios:
            #suma  
            totalservicios = servi.costo + totalservicios
            
                
        print('totalservicios')
        print(totalservicios)

        #restante global resta 
        #filtramos por pk para poder actualizar los datos 
        grantotal = int(totalservicios)
        resta =  grantotal - int(instance.proyecto.adelanto)
        print('resta global')
        print(resta)

        #filtramos por pk para poder actualizar los datos 
        Proyect.objects.filter(pk=instance.proyecto.id).update(total=grantotal, restante=resta)

# agregamos el parámetro sender con la clase PagosProyecto
# para indicarle a donde tiene que escuchar.
@receiver(post_save, sender=PagosProyecto)
def post_save_pago_proyect(sender, instance, created, **kwargs):

    # Verifico que se crea un pagoproyecto
    if created:
        #filtarmos por pago y sacamos el id de proyecto
        pagopro = PagosProyecto.objects.filter(proyecto_id=instance.proyecto.id, estatus=2)
        #iteramos para poder sacar la suma total
        

        totalpagoproyect = 0 

        for pago  in   pagopro:
            #suma  
            totalpagoproyect = pago.monto + totalpagoproyect
            
                
        print('totalpagoproyect')
        print(totalpagoproyect)

        #restante global resta 

        grantotal = int(instance.proyecto.total)
        resta =  grantotal - int(totalpagoproyect)
        print('resta global')
        print(resta)

        #filtramos por pk para poder actualizar los datos 
        Proyect.objects.filter(pk=instance.proyecto.id).update(total=grantotal, adelanto=totalpagoproyect, restante=resta,)

    else:
        
   
        pagopro = PagosProyecto.objects.filter(proyecto_id=instance.proyecto.id, estatus=2)
        #iteramos para poder sacar la suma total
        

        totalpagoproyect = 0 

        for pago  in   pagopro:
            #suma  
            totalpagoproyect = pago.monto + totalpagoproyect
            
                
        print('totalpagoproyect')
        print(totalpagoproyect)

        #restante global resta 
        grantotal = int(instance.proyecto.monto) + int(instance.proyecto.extras)
        resta =  grantotal - int(totalpagoproyect)
        print('resta global')
        print(resta)

        #filtramos por pk para poder actualizar los datos 
        Proyect.objects.filter(pk=instance.proyecto.id).update(total=grantotal, adelanto=totalpagoproyect, restante=resta,)


@receiver(post_delete, sender=PagosProyecto)
def delete_pagopro(sender, instance, **kwargs):

    pagopro = PagosProyecto.objects.filter(proyecto_id=instance.proyecto.id, estatus=2)
    #iteramos para poder sacar la suma total
        

    totalpagoproyect = 0 

    for pago  in   pagopro:
        #suma  
        totalpagoproyect = pago.monto + totalpagoproyect
        
            
    print('totalpagoproyect')
    print(totalpagoproyect)

    #restante global resta 
    grantotal = int(instance.proyecto.monto) + int(instance.proyecto.extras)
    resta =  grantotal - int(totalpagoproyect)
    print('resta global')
    print(resta)

    #filtramos por pk para poder actualizar los datos 
    Proyect.objects.filter(pk=instance.proyecto.id).update(total=grantotal, adelanto=totalpagoproyect, restante=resta,)


# agregamos el parámetro sender con la clase HistorialCampana
# para indicarle a donde tiene que escuchar.
@receiver(post_save, sender=HistorialCampana)
def post_save_historial_campa(sender, instance, created, **kwargs):

    # Verifico que se crea un HistorialCampana
    if created:
        #filtarmos por campa y sacamos el id de proyecto
        histocampa = HistorialCampana.objects.filter(proyecto_id=instance.proyecto.id,)
        #iteramos para poder sacar la suma total
        

        totalcampana = 0 

        for campa  in   histocampa:
            #suma  
            totalcampana = campa.inversion + totalcampana
            
                
        print('totalcampana')
        print(totalcampana)

        #restante global resta 
        resta =  int(instance.proyecto.total) - int(totalcampana)
        print('resta global')
        print(resta)

        #filtramos por proyectopubli por pk para poder actualizar los datos 
        ProyectoPublicidad.objects.filter(pk=instance.proyecto.id).update(adelanto=totalcampana, restante=resta,)

    else:
        
   
        histocampa = HistorialCampana.objects.filter(proyecto_id=instance.proyecto.id,)
        #iteramos para poder sacar la suma total
        

        totalcampana = 0 

        for campa  in   histocampa:
            #suma  
            totalcampana = campa.inversion + totalcampana
            
                
        print('totalcampana')
        print(totalcampana)

        #restante global resta 
        resta =  int(instance.proyecto.total) - int(totalcampana)
        print('resta global')
        print(resta)

         #filtramos por proyectopubli por pk para poder actualizar los datos 
        ProyectoPublicidad.objects.filter(pk=instance.proyecto.id).update(adelanto=totalcampana, restante=resta,)


@receiver(post_delete, sender=HistorialCampana)
def delete_pagopro(sender, instance, **kwargs):

        histocampa = HistorialCampana.objects.filter(proyecto_id=instance.proyecto.id,)
        #iteramos para poder sacar la suma total
        

        totalcampana = 0 

        for campa  in   histocampa:
            #suma  
            totalcampana = campa.inversion + totalcampana
            
                
        print('totalcampana')
        print(totalcampana)

        #restante global resta 
        resta =  int(instance.proyecto.total) - int(totalcampana)
        print('resta global')
        print(resta)

         #filtramos por proyectopubli por pk para poder actualizar los datos 
        ProyectoPublicidad.objects.filter(pk=instance.proyecto.id).update(adelanto=totalcampana, restante=resta,)