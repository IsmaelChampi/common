from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.urls import path
from django.contrib.auth import views as auth_views
from proyectos import views
import csv
from django.http import HttpResponse
from django.contrib.auth.models import User

from proyectos.views import  homeview, search



urlpatterns = [

    # Posts
    path('add_proyecto', views.add_proyecto, name='add_proyecto'),

    path('add_proyecto_cliente/<int:pk>/', views.add_proyecto_cliente, name='add_proyecto_cliente'),
    path('add_tag', views.add_tag, name='add_tag'), 
    path('list_tag', views.list_tag, name='list_tag'),
    path('edit_tag/<int:pk>/', views.edit_tag, name='edit_tag'),
    path('ajax_add_proyecto', views.ajax_add_proyecto, name='ajax_add_proyecto'),
    path('ajax_add_proyecto_cliente', views.ajax_add_proyecto, name='ajax_add_proyecto'),
    path('list_proyecto', views.list_proyecto, name='list_proyecto'),
    path('edit_proyecto/<int:pk>/', views.edit_proyecto, name='edit_proyecto'),
    path('detail_proyecto/<int:pk>/', views.detail_proyecto, name='detail_proyecto'),
    path('tarea_proyecto/<int:pk>/', views.tarea_proyecto, name='tarea_proyecto'),
    path('add_categoria', views.add_categoria, name='add_categoria'),
    path('list_categoria', views.list_categoria, name='list_categoria'),
    path('detail_categoria/<int:pk>/', views.detail_categoria, name='detail_categoria'),
    path('edit_categoria/<int:pk>/', views.edit_categoria, name='edit_categoria'),
    path('add_dis/<int:pk>/', views.add_dis, name='add_dis'),
    path('list_dis', views.list_dis, name='list_dis'),
    path('detail_dis/<int:pk>/', views.detail_dis, name='detail_dis'),
    path('edit_dis/<int:pk>/', views.edit_dis, name='edit_dis'),
    path('add_tarea/<int:pk>/', views.add_tarea, name='add_tarea'),
    path('list_tarea', views.list_tarea, name='list_tarea'),
    path('edit_tarea/<int:pk>/', views.edit_tarea, name='edit_tarea'),
    path('detail_tarea/<int:pk>/', views.detail_tarea, name='detail_tarea'),
    path('add_comentare/<int:pk>/', views.add_comentare, name='add_comentare'),
    path('list_comentare', views.list_comentare, name='list_comentare'),
    path('edit_comentare/<int:pk>/', views.edit_comentare, name='edit_comentare'),
    path('detail_comentare/<int:pk>/', views.detail_comentare, name='detail_comentare'),
    path('add_imagetarea/<int:pk>/', views.add_imagetarea, name='add_imagetarea'),
    path('add_servicio/<int:pk>/', views.add_servicio, name='add_servicio'),
    path('list_servicio', views.list_servicio, name='list_servicio'),
    path('edit_servicio/<int:pk>/', views.edit_servicio, name='edit_servicio'),
    path('detail_servicio/<int:pk>/', views.detail_servicio, name='detail_servicio'),
    path('add_pagopro/<int:pk>/', views.add_pagopro, name='add_pagopro'),
    path('list_pagopro', views.list_pagopro, name='list_pagopro'),
    path('edit_pagopro/<int:pk>/', views.edit_pagopro, name='edit_pagopro'),
    path('detail_pagopro/<int:pk>/', views.detail_pagopro, name='detail_pagopro'),
    path('add_publi/<int:pk>/', views.add_publi, name='add_publi'),
    path('list_publi', views.list_publi, name='list_publi'),
    path('edit_publi/<int:pk>/', views.edit_publi, name='edit_publi'),
    path('detail_publi/<int:pk>/', views.detail_publi, name='detail_publi'),
    path('add_campa/<int:pk>/', views.add_campa, name='add_campa'),
    path('list_campa', views.list_campa, name='list_campa'),
    path('edit_campa/<int:pk>/', views.edit_campa, name='edit_campa'),
    path('detail_campa/<int:pk>/', views.detail_campa, name='detail_campa'),
    path('search/', search.as_view(), name='search'),
    path('homeview/', homeview.as_view(), name='homeview'),
    path('tarea_detail/<int:pk>/<int:estatus>/', views.tarea_detail, name='tarea_detail'),
    path('get_tarea', views.get_tarea, name='get_tarea'),
    path('ajax_edit_tarea/<int:pk>/', views.ajax_edit_tarea, name='ajax_edit_tarea'),
    path('ajax_edit_comentare/<int:pk>/', views.ajax_edit_comentare, name='ajax_edit_comentare'),
    path('ajax_edit_dis/<int:pk>/', views.ajax_edit_dis, name='ajax_edit_dis'), 
    path('ajax_fill_servicio/<int:pk>/', views.ajax_fill_servicio, name='ajax_fill_servicio'),
    path('ajax_edit_pagopro/<int:pk>/', views.ajax_edit_pagopro, name='ajax_edit_pagopro'),
    path('ajax_edit_publi/<int:pk>/', views.ajax_edit_publi, name='ajax_edit_publi'),
    path('ajax_edit_campa/<int:pk>/', views.ajax_edit_campa, name='ajax_edit_campa'),  
    path('ajax_list_comentare/<int:pk>/', views.ajax_list_comentare, name='ajax_list_comentare'), 
    path('ajax_list_campa/<int:pk>/', views.ajax_list_campa, name='ajax_list_campa'), 
    path('export_proyect', views.export_proyect, name='export_proyect'),
    path('export_tarea', views.export_tarea, name='export_tarea'),
    path('export_pagopro', views.export_pagopro, name='export_pagopro'),
    path('ajax_save_campa/<int:pk>/', views.ajax_save_campa, name='ajax_save_campa'),
    path('ajax_save_publi/<int:pk>/', views.ajax_save_publi, name='ajax_save_publi'),
    path('ajax_save_pagopro/<int:pk>/', views.ajax_save_pagopro, name='ajax_save_pagopro'),
    path('ajax_save_servicio/<int:pk>/', views.ajax_save_servicio, name='ajax_save_servicio'),
    path('ajax_save_dis/<int:pk>/', views.ajax_save_dis, name='ajax_save_dis'), 
    path('ajax_save_comentarea/<int:pk>/', views.ajax_save_comentarea, name='ajax_save_comentarea'),
    path('ajax_save_tarea/<int:pk>/', views.ajax_save_tarea, name='ajax_save_tarea'),
    path('ajax_fill_tarea/<int:pk>/', views.ajax_fill_tarea, name='ajax_fill_tarea'), 
    path('ajax_save_fill_tarea/<int:pk>/', views.ajax_save_fill_tarea, name='ajax_save_fill_tarea'),
    path('ajax_save_cometario/', views.ajax_save_cometario, name='ajax_save_cometario'),
    path('imagen/', views.BasicUploadView.as_view(), name='basic_upload'),
    path('ajax_save_fill_servicio/<int:pk>/', views.ajax_save_fill_servicio, name='ajax_save_fill_servicio'), 
    path('ajax_save_fill_pagopro/<int:pk>/', views.ajax_save_fill_pagopro, name='ajax_save_fill_pagopro'), 
    path('ajax_fill_pagopro/<int:pk>/', views.ajax_fill_pagopro, name='ajax_fill_pagopro'),
    path('ajax_save_fill_publi/<int:pk>/', views.ajax_save_fill_publi, name='ajax_save_fill_publi'), 
    path('ajax_fill_publi/<int:pk>/', views.ajax_fill_publi, name='ajax_fill_publi'),
    path('ajax_save_fill_campa/<int:pk>/', views.ajax_save_fill_campa, name='ajax_save_fill_campa'), 
    path('ajax_fill_campa/<int:pk>/', views.ajax_fill_campa, name='ajax_fill_campa'),
    path('detail_propubli/<int:pk>/', views.detail_proyectopubli, name='detail_proyectopubli'),
    path('filtro_tag_proyecto/<int:pk>/', views.filtro_tag_proyecto, name='filtro_tag_proyecto'),
    path('filtro_tag_cliente/<int:pk>/', views.filtro_tag_cliente, name='filtro_tag_cliente'),
    path('ajax_list_filtrotagproyect/<int:pk>/', views.ajax_list_filtrotagproyect, name='ajax_list_filtrotagproyect'), 
    path('ajax_list_filtrotagclient/<int:pk>/', views.ajax_list_filtrotagclient, name='ajax_list_filtrotagclient'),
    path('filtro_tag/<int:pk>/', views.filtro_tag, name='filtro_tag'),

    #
    path('ajax_change_proyect_status/<int:pk>/', views.ajax_change_proyect_status, name='ajax_change_proyect_status'), 
    path('ajax_list_photos/<int:pk>/', views.ajax_list_photos, name='ajax_list_photos'), 
    path('ajax_set_img_pk/<int:pk>/', views.ajax_set_img_pk, name='ajax_set_img_pk'), 

    #
     path('my_projects', views.my_projects, name='my_projects'),

     path('filtro', views.filtro.as_view(), name='filtro'), 



    
    
    

   


]