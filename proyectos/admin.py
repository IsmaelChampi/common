from django.contrib import admin
from proyectos.models import Categoria, Discusiones, Proyect, Tareas, ComentariosTarea, ImageTarea,  ServiciosExtras, PagosProyecto, ProyectoPublicidad, HistorialCampana, Tag

# Register your models here.


# Register your models here.
@admin.register(Proyect)
class ProyectAdmin(admin.ModelAdmin):
    list_display = ('usuario', 'categoria')
    list_per_page = 30

@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
    list_display = ('nombre',)
    list_per_page = 30

@admin.register(Discusiones)
class DiscusionesAdmin(admin.ModelAdmin):
    list_display = ('proyecto',)
    list_per_page = 30


@admin.register(Tareas)
class TareasAdmin(admin.ModelAdmin):
    list_display = ('titulo',)
    list_per_page = 30


@admin.register(ComentariosTarea)
class ComentariosTareaAdmin(admin.ModelAdmin):
    list_display = ('tarea',)
    list_per_page = 30


@admin.register(ImageTarea)
class ImageTarea(admin.ModelAdmin):
    list_per_page = 30


@admin.register(ServiciosExtras)
class ServiciosExtrasAdmin(admin.ModelAdmin):
    list_display = ('titulo',)
    list_per_page = 30


@admin.register(PagosProyecto)
class PagosProyectoAdmin(admin.ModelAdmin):
    list_display = ('comentario',)
    list_per_page = 30 


@admin.register(ProyectoPublicidad)
class ProyectoPublicidadAdmin(admin.ModelAdmin):
    list_display = ('nombre',)
    list_per_page = 30

@admin.register(HistorialCampana)
class HistorialCampanaAdmin(admin.ModelAdmin):
    list_display = ('comentario',)
    list_per_page = 30

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('color',)
    list_per_page = 30