# Generated by Django 2.2.6 on 2020-04-07 05:05

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('proyectos', '0029_auto_20200407_0411'),
    ]

    operations = [
        migrations.AddField(
            model_name='proyect',
            name='fecha',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now, verbose_name='Fecha'),
            preserve_default=False,
        ),
    ]
