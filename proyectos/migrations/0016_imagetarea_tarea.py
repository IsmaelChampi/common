# Generated by Django 2.2.6 on 2020-01-10 17:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('proyectos', '0015_auto_20200110_0653'),
    ]

    operations = [
        migrations.AddField(
            model_name='imagetarea',
            name='tarea',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='proyectos.Tareas'),
            preserve_default=False,
        ),
    ]
