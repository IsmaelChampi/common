# Generated by Django 2.2.6 on 2020-04-13 05:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proyectos', '0041_auto_20200411_0345'),
    ]

    operations = [
        migrations.AlterField(
            model_name='proyect',
            name='adelanto',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='proyect',
            name='extras',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='proyect',
            name='monto',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='proyect',
            name='restante',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='proyect',
            name='total',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='proyectopublicidad',
            name='adelanto',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='proyectopublicidad',
            name='restante',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='proyectopublicidad',
            name='total',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
    ]
