# Generated by Django 2.2.6 on 2019-10-30 19:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('proyectos', '0003_auto_20191028_2258'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImageTarea',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('imagen', models.ImageField(default='media/None/no-img.jpg', upload_to='media/')),
                ('tarea', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='proyectos.Tareas')),
            ],
        ),
    ]
