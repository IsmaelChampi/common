# Generated by Django 2.2.6 on 2020-04-07 04:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proyectos', '0028_auto_20200407_0344'),
    ]

    operations = [
        migrations.AddField(
            model_name='proyect',
            name='avance',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='proyect',
            name='estado',
            field=models.IntegerField(blank=True, choices=[(1, 'Pendiente'), (2, 'Proceso'), (3, 'Completado'), (4, 'Cancelado')], null=True, verbose_name='estado'),
        ),
    ]
