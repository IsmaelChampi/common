from django import forms
from proyectos.models import Categoria, Discusiones, Proyect, Tareas, ComentariosTarea, ImageTarea, ServiciosExtras, PagosProyecto, ProyectoPublicidad, HistorialCampana, Tag
from common.models import Perfil
from django.utils.translation import ugettext as _


from clientes.models import Client
class CategoriaForm(forms.ModelForm):
    

    nombre = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Nombre(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Nombre')},
        label=_('Nombre(s)'),
        required=True,
    )

    class Meta:
        model = Categoria
        fields = ('nombre',)


class TagForm(forms.ModelForm):
    

    nombre = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Nombre(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Nombre')},
        label=_('Nombre(s)'),
        required=True,
    )

    color = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Color(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese color')},
        label=_('Color(s)'),
        required=True,
    )

    class Meta:
        model = Tag
        fields = ('nombre','color',)
       
class ProyectForm(forms.ModelForm):
    

    nombre = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Nombre(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Nombre')},
        label=_('Nombre(s)'),
        required=True,
    )


    categoria = forms.ModelChoiceField(
        queryset=Categoria.objects.all(), 
        widget=forms.Select( attrs={'class': "form-control"}),
    )

    usuario = forms.ModelChoiceField(
        queryset=Perfil.objects.all(), 
        widget=forms.Select( attrs={'class': "form-control"}),
    )

    cliente = forms.ModelChoiceField(
        queryset=Client.objects.all(), 
        widget=forms.Select( attrs={'class': "form-control"}),
    )

    detalles = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder': _('Detalles'), 'class': "form-control"}),
        max_length=1000,
        error_messages={'required': _(u'Detalles')},
        label=_('Detalles'),
        required=True,
    )

    hosting_options = (
        (1, 'Interno'),
        (2, 'Externo'),
    )

    hosting = forms.ChoiceField(widget=forms.Select(attrs={'placeholder': _('Hospedaje'), 'class': "form-control"}), choices=hosting_options,required=False,)

    size_options = (
        (1, '5 GB'),
        (2, '25 GB'),
        (3, '50 GB'),
        (4, '75 GB'),
    )
    size_hosting = forms.ChoiceField(widget=forms.Select(attrs={'placeholder': _('Tamaño Hospedaje'), 'class': "form-control"}), choices=size_options,required=False,)


    datos_hosting = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Datos Hospedaje'), 'class': "form-control"}),
        required=False,
    )


    dominio_options = (
        (1, 'Interno'),
        (2, 'Externo'),
    )

    dominio = forms.ChoiceField(widget=forms.Select(attrs={'placeholder': _('Hospedaje'), 'class': "form-control"}), choices=dominio_options,required=False,)
    datos_dominio = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Datos Dominio'), 'class': "form-control"}),
        required=False,
    )



    """tag = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Tag(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Tag')},
        label=_('Tag(s)'),
        required=True,
    )"""

    class Meta:
        model = Proyect
        fields = ('nombre', 'categoria', 'usuario', 'detalles', 'hosting','size_hosting', 'datos_hosting', 'dominio', 'datos_dominio')



class CatForm(forms.ModelForm):
    
    categoria = forms.ModelChoiceField(
        queryset=Categoria.objects.all(), 
        widget=forms.Select( attrs={'placeholder': _('Categoría'),'class': "form-control"}),
        required=False,
        label=_(''),
    )

    class Meta:
        model = Proyect
        fields = ('categoria',)



class DiscusionesForm(forms.ModelForm):
    

    asunto = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Asunto(s)'), 'class': "form-control"}),
        max_length=100,
        error_messages={'required': _(u'Ingrese Asunto')},
        label=_('Asunto(s)'),
        required=True,
    )
    descripcion = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder': _('Descripcion'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Descripcion')},
        label=_('Descripcion'),
        required=True,
    )

    class Meta:
        model = Discusiones
        fields = ('asunto', 'descripcion', 'proyecto', 'usuario',)


class TareasForm(forms.ModelForm):

    titulo = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Titulo'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Titulo')},
        label=_('Titulo(s)'),
        required=True,
    )

    detalles = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder': _('Descripción'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Descripción')},
        label=_('Descripción'),
        required=True,
    )

    owner = forms.ModelChoiceField(
        queryset=Perfil.objects.all(), 
        widget=forms.Select( attrs={'class': "form-control",'readonly':'readonly'}),
         label=_('Creador'),
    )

    usuario = forms.ModelChoiceField(
        queryset=Perfil.objects.all(), 
        widget=forms.Select( attrs={'class': "form-control"}),
         label=_('Asignado a:'),
    )

    fechav = forms.DateField(
        widget=forms.DateInput(attrs={'placeholder': _('Fecha Vencimiento'), 'class': "form-control"}),
        label=_('Fecha Vencimiento'),
    )

    CHOICES= (
        (1, 'Pendiente'),
        (2, 'Proceso'),
        (3, 'Completado'),
        (4, 'Cancelado'),
        )

    estado = forms.ChoiceField(
        widget=forms.Select( attrs={'class': "form-control"}),
        choices=CHOICES,
        error_messages={'required': _(u'Seleccion tu Estado')},
        label=_('Estado'),
        required=True,
    )

    CHOICES= (
        (1, 'Alta'),
        (2, 'Media'),
        (3, 'Baja'),
        )

    prioridad = forms.ChoiceField(
        widget=forms.Select( attrs={'class': "form-control"}),
        choices=CHOICES,
        error_messages={'required': _(u'Seleccion su prioridad')},
        label=_('Prioridad'),
        required=True,
    )

    orden = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Orden'), 'class': "form-control"}),
        error_messages={'required': _(u'Orden')},
        label=_('Orden'),
        required=True,
    )


    class Meta:
        model = Tareas
        fields = ('titulo', 'detalles','owner', 'usuario', 'fechav', 'estado','usuario', 'proyecto', 'prioridad', 'orden',)
       
class ComentariosTareaForm(forms.ModelForm):

    descripcion = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder': _('Descripcion'), 'class': "form-control", 'rows':"5",}),
        max_length=1000,
        error_messages={'required': _(u'Ingrese Descripcion')},
        label=_('Descripcion'),
        required=True,
    )
    class Meta:
        model = ComentariosTarea
        fields = ('descripcion', 'usuario', 'tarea')


class ImageTareaForm(forms.ModelForm):
    class Meta:
        model = ImageTarea
        fields = ('file', 'tarea',)

class ServiciosExtrasForm(forms.ModelForm):

    titulo = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Concepto'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Titulo')},
        label=_('Titulo(s)'),
        required=True,
    )

    descripcion = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder': _('Descripcion'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Descripcion')},
        label=_('Descripcion'),
        required=True,
    )


    costo = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Costo'), 'class': "form-control"}),
        error_messages={'required': _(u'Costo')},
        label=_('Costo'),
        required=True,
    )

    tipo_options = (
        (1, 'Servicio Inicial'),
        (2, 'Servicio Extra'),
       
        
        )

    tipo = forms.ChoiceField(widget=forms.Select(attrs={'placeholder': _('Tipo'), 'class': "form-control"}), choices=tipo_options,required=False,)



    class Meta:
        model = ServiciosExtras
        fields = ('titulo', 'descripcion', 'costo', 'proyecto',)


class PagosProyectoForm(forms.ModelForm):

    monto = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Monto'), 'class': "form-control"}),
        error_messages={'required': _(u'Monto')},
        label=_('Monto'),
        required=True,
    )

    comentario = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Comentario'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Comentario')},
        label=_('Comentario'),
        required=True,
    )
    CHOICES= (
        (1, 'pendiente'),
        (2, 'confirmado'),
        (3, 'cancelado'),
        )

    estatus = forms.ChoiceField(
        widget=forms.Select( attrs={'class': "form-control"}),
        choices=CHOICES,
        error_messages={'required': _(u'Seleccion Estatus')},
        label=_('Estatus'),
        required=True,
    )
    
    class Meta:
        model = PagosProyecto
        fields = ('monto', 'comentario', 'proyecto', 'estatus',)


class ProyectoPublicidadForm(forms.ModelForm):

    CHOICES= (
        (1, 'Programada'),
        (2, 'Activa'),
        (3, 'Terminada'),
        (4, 'Cancelada'),
        )

    estado = forms.ChoiceField(
        widget=forms.Select( attrs={'class': "form-control"}),
        choices=CHOICES,
        error_messages={'required': _(u'Seleccion tu Estado')},
        label=_('Estado'),
        required=True,
    )


    empresa = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Empresa(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Empresa')},
        label=_('Empresa(s)'),
        required=True,
    )

    idpublicidad = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('ID-publicidad(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese id')},
        label=_('ID-publicidad(s)'),
        required=True,
    )

    promocion = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Promocion(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Promocion')},
        label=_('Promocion(s)'),
        required=True,
    )
    
    class Meta:
        model = ProyectoPublicidad
        fields = ('estado', 'nombre', 'empresa', 'idpublicidad', 'promocion', 'usuario',  'total', 'adelanto', 'restante',)


class HistorialCampanaForm(forms.ModelForm):

    fechaini = forms.DateField(
        label=_('Fecha'),
    )

    fechater = forms.DateField(
        label=_('Fecha'),
    )

    inversion = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Inversion'), 'class': "form-control"}),
        error_messages={'required': _(u'Inversion')},
        label=_('Inversion'),
        required=True,
    )

    comision = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Comision'), 'class': "form-control"}),
        error_messages={'required': _(u'Comision')},
        label=_('Comision'),
        required=True,
    )

    comentario = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Comentario'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Comentario')},
        label=_('Comentario'),
        required=True,
    )
    
    class Meta:
        model = HistorialCampana
        fields = ('fechaini', 'fechater', 'inversion', 'comision', 'comentario', 'proyecto',)
