from django.shortcuts import render, redirect
from proyectos.forms import CategoriaForm, ProyectForm, DiscusionesForm, TareasForm, ComentariosTareaForm, ImageTareaForm, ServiciosExtrasForm, PagosProyectoForm, ProyectoPublicidadForm, HistorialCampanaForm, Tag, CatForm
from proyectos.models import Proyect, Discusiones, Categoria, Tareas, ComentariosTarea, ImageTarea, ServiciosExtras, PagosProyecto, ProyectoPublicidad, HistorialCampana, Tag
from common.models import Perfil
from clientes.models import Client
from django.db.models import Q 
from django.core.mail import send_mail
from django.conf.urls import include, url
from django.views.generic import TemplateView, ListView
import csv
from django.http import HttpResponse
from django.views import View
from django.http import JsonResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from django.utils.decorators import method_decorator
#from common.decorators import group_required


# Create your views here.


from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse


from django.views.decorators.csrf import csrf_exempt


from django.http import HttpResponse, JsonResponse
import json
from django.views import View


@login_required(login_url='/login')
def add_proyecto(request):

    
    if request.method == 'POST':
        form = ProyectForm(request.POST) 
        if form.is_valid():
            form.save()

            """send_mail('hello ismael',
        'hello this is automated messague.',
        'ismael.hernandez@upam.edu.mx',
        ['ismael@ticsup.com'],
        fail_silently=False)"""


  
            return redirect('home')

    else:
        
        form = ProyectForm(initial={'usuario': request.user,}) #para crer un nuevo objeto
        form.fields['usuario'].widget = forms.HiddenInput()
        

      

    return render(
        request=request,
        template_name='add_proyecto.html',
        context={
            
            'form': form
        })


@login_required(login_url='/login')
def add_proyecto_cliente(request,pk):

    cliente = Client.objects.get(pk=pk)
    
    if request.method == 'POST':
        form = ProyectForm(request.POST) 
        if form.is_valid():
            form.save()

            """send_mail('hello ismael',
        'hello this is automated messague.',
        'ismael.hernandez@upam.edu.mx',
        ['ismael@ticsup.com'],
        fail_silently=False)"""


  
            return redirect('home')

    else:
        
        form = ProyectForm(initial={'usuario': request.user, 'cliente': cliente,}) #para crer un nuevo objeto
        form.fields['usuario'].widget = forms.HiddenInput()
        form.fields['cliente'].widget = forms.HiddenInput()
        

      

    return render(
        request=request,
        template_name='add_proyecto_cliente.html',
        context={
            
            'form': form
        })


# agregar proyecto mediante ajax
@login_required(login_url='/login')
@csrf_exempt
def ajax_add_proyecto(request):
    data = request.body
    data = json.loads(data)
    nombre = (data['nombre'])
    usuario_id = (data['usuario'])
    cliente_id = (data['cliente'])
    categoria = (data['categoria']) 
    detalles = (data['detalles'])
    hosting = (data['hosting'])
    datos_hosting = (data['datos_hosting'])
    dominio = (data['dominio'])
    datos_dominio = (data['datos_dominio'])
    size_hosting = (data['size_hosting'])


    proyecto = Proyect.objects.create(nombre=nombre, client_id=cliente_id,usuario_id=usuario_id, categoria_id=categoria, detalles=detalles, hosting=hosting, datos_hosting=datos_hosting, dominio=dominio, datos_dominio=datos_dominio, size_hosting=size_hosting)
    
    response = JsonResponse({ 'success': 1, 'id': proyecto.id,})
    return response 


@login_required(login_url='/login')
@csrf_exempt
def ajax_add_proyecto_cliente(request):
    data = request.body
    data = json.loads(data)
    nombre = (data['nombre'])
    usuario_id = (data['usuario'])
    cliente_id = (data['cliente'])
    categoria = (data['categoria']) 
    detalles = (data['detalles'])
    monto = (data['monto'])
    proyecto = Proyect.objects.create(nombre=nombre, client_id=cliente_id,usuario_id=usuario_id, categoria_id=categoria, detalles=detalles, monto=monto)
    
    response = JsonResponse({ 'success': 1, 'id': proyecto.id,})
    return response 


@login_required(login_url='/login')
def list_proyecto(request):

    proyectos = Proyect.objects.all()
    formc = CatForm() #nuevo objeto

    return render(
        request=request,
        template_name='list_proyecto.html',
        context={
             'proyectos': proyectos, 'form':formc,
          
        })



@login_required(login_url='/login')
def my_projects(request):

    proyectos = Proyect.objects.filter(usuario=request.user)

    return render(
        request=request,
        template_name='list_my_proyecto.html',
        context={
             'proyectos': proyectos,
          
        })



@method_decorator(login_required, name='dispatch')
#@method_decorator(group_required('Administrador'), name='dispatch')
class filtro(ListView): 
    model = Proyect
    template_name = 'filtro.html'
    

# funcion buscador de proyectos 
    def get_queryset(self):
        nombre = self.request.GET.get('nombre', None)
        cat = self.request.GET.get('categoria', None)
        print('query', nombre)
        print('query cat', cat)
        try:
            if nombre != '' and cat != '':
                print('not 1')
                listpro = Proyect.objects.filter(Q(nombre__contains=nombre, categoria_id=cat))
            elif nombre != '' and cat== '':
                print('not 2')
                listpro = Proyect.objects.filter(Q(nombre__contains=nombre))
            elif nombre == '' and cat != '':
                print('not 3')
                listpro = Proyect.objects.filter(categoria_id=cat)

            elif nombre== '' and cat== '':
                print('ambos null')
                listpro = []


        except:
            print('except')
            listpro = Proyect.objects.all()

        print('listpro, ', listpro)
     

        return listpro


    def get_context_data(self, **kwargs):
        ctx = super(filtro, self).get_context_data(**kwargs)
        formc = CatForm() #nuevo objeto
        ctx['form'] = formc
        return ctx





@login_required(login_url='/login')
def edit_proyecto(request, pk):
    #instancia
    proyecto = Proyect.objects.get(pk=pk)
    # Creamos formulario con los datos de la instancia
    form = ProyectForm(request.POST, instance=proyecto)  #objeto
    #comprobamos formulario
    if request.method == 'POST':
        # Actualizamos el formulario con los datos recibidos
        form = ProyectForm(request.POST, instance=proyecto)
        # Si el formulario es válido
        if form.is_valid():
            form.save()
            proyecto = form.save(commit=False)
            proyecto.save()
            return redirect('home')
    else:
        form = ProyectForm() #nuevo objeto
       

    return render(
        request=request,
        template_name='edit_proyecto.html',
        context={
            
            'form': form,
            'proyecto': proyecto,
        })

@login_required(login_url='/login')
def detail_proyecto(request, pk):


    proyectos = Proyect.objects.get(pk=pk)

    discusiones =  Discusiones.objects.filter(proyecto = proyectos).order_by('-fecha')
    servicios = ServiciosExtras.objects.filter(proyecto = proyectos).order_by('-fecha')
    pagos = PagosProyecto.objects.filter(proyecto = proyectos).order_by('-fecha')
    tareas = Tareas.objects.filter(proyecto = proyectos).order_by('-fechac')

    archivos = ImageTarea.objects.filter(tarea__proyecto = proyectos)

    #print('avance calc')
    #print(tareas_completado)
    #print(avance)

    #form = DiscusionesForm() #para crer un nuevo objeto
    formdis = DiscusionesForm(initial={'usuario': request.user, 'proyecto': proyectos,})
    formdis.fields['usuario'].widget = forms.HiddenInput()
    formdis.fields['proyecto'].widget = forms.HiddenInput()
    #form = DiscusionesForm() #para crer un nuevo objeto
    formser = ServiciosExtrasForm(initial={'proyecto': proyectos,})
    formser.fields['proyecto'].widget = forms.HiddenInput()
    #form = DiscusionesForm() #para crer un nuevo objeto
    formpa = PagosProyectoForm(initial={'proyecto': proyectos,})
    formpa.fields['proyecto'].widget = forms.HiddenInput()


    #form = add tarea formesForm() #para crer un nuevo objeto
    formta = TareasForm(initial={'usuario': request.user, 'proyecto': proyectos,})
    formta.fields['proyecto'].widget = forms.HiddenInput()
    formta.fields['usuario'].widget = forms.HiddenInput()
    #forms comentario


    return render(
        request=request,
        template_name='detail_proyecto.html',
        context={
            'proyectos': proyectos, 'tareas': tareas, 'formdis': formdis, 'discusiones': discusiones, 'servicios': servicios, 'pagos': pagos, 'tareas': tareas, 'formta': formta, 'formser': formser, 'formpa': formpa,
            'archivos':archivos,
          
        })




@login_required(login_url='/login')
def add_categoria(request):
    if request.method == 'POST':
        form = CategoriaForm(request.POST) 
        if form.is_valid():
            form.save()

  
            return redirect('home')

    else:
    	form = CategoriaForm() #para crer un nuevo objeto
      

    return render(
        request=request,
        template_name='add_categoria.html',
        context={
            
            'form': form
        })

@login_required(login_url='/login')
def add_tag(request):
    if request.method == 'POST':
        formtag = TagForm(request.POST) 
        if formtag.is_valid():
            formtag.save()

  
            return redirect('home')

    else:
        formtag = TagForm() #para crer un nuevo objeto
      

    return render(
        request=request,
        template_name='add_tag.html',
        context={
            
            'formtag': formtag
        })


@login_required(login_url='/login')
def list_tag(request):

    
    tags = Tag.objects.all()

    return render(
        request=request,
        template_name='list_tag.html',
        context={
             'tags': tags,
             
            
        })


def filtro_tag_proyecto(request, pk):
    tag = Tag.objects.get(pk=pk)
    proyectos = Proyect.objects.filter(tag=tag)

    print(proyectos)
    print(tag)


    return render(
        request=request,
        template_name='detail_tagproyect.html',
        context={
             'tag': tag,
             'proyectos': proyectos,
             
            
        })

def filtro_tag_cliente(request, pk):
    tag = Tag.objects.get(pk=pk)
    clientes = Client.objects.filter(tag=tag)

    print(clientes)
    print(tag)


    return render(
        request=request,
        template_name='detail_tag.html',
        context={
             'tag': tag,
             'clientes': clientes,
             
            
        })

def filtro_tag(request, pk):
    tag = Tag.objects.get(pk=pk)
    proyectos = Proyect.objects.filter(tag=tag)
    clientes = Client.objects.filter(tag=tag)
    

    print(clientes)
    print(tag)


    return render(
        request=request,
        template_name='listtag.html',
        context={
             'tag': tag,
             'clientes': clientes,
             'proyectos': proyectos,
             
            
        })

#listar proyecto-tag mediante ajax
@login_required(login_url='/login')
@csrf_exempt
def ajax_list_filtrotagproyect(request, pk):
    tag = Tag.objects.get(pk=pk)#sera la relacion del pk de tarea
    proyectos = Proyect.objects.filter(tag=tag)

    print('entro vista ajax')
    listproyectag =[]
    for  tagproyect in  proyectos:
        #crear un diccionario con los datos de comentariotarea, 
        protagdic = {
            'nombre': tagproyect.nombre,
            'categoria': str(tagproyect.categoria),  
            'usuario': tagproyect.usuario.pk,
            'detalles': tagproyect.detalles,
            
            
            }

        #metodo append de agregar diccionario
        listproyectag.append(protagdic)


    response = json.dumps(listproyectag,  ensure_ascii=False)
    print(response)
    return JsonResponse(response, safe=False) 

#listar cliente-tag mediante ajax

@login_required(login_url='/login')
@csrf_exempt
def ajax_list_filtrotagclient(request, pk):
    tag = Tag.objects.get(pk=pk)#sera la relacion del pk de tarea
    clientes = Client.objects.filter(tag=tag)

    print('entro vista ajax')
    listclientctag =[]
    for  tagclient in  clientes:
        #crear un diccionario con los datos de comentariotarea, 
        clientagdic = {
            'nombre': tagclient.nombre,
            'email_Corporativo': tagclient.email_Corporativo,  
            'email_Personal': tagclient.email_Personal,
            'telefono': tagclient.telefono,
            'empresa': tagclient.empresa,
            'ciudad': tagclient.ciudad,
           
            
            
            }

        #metodo append de agregar diccionario
        listclientctag.append(clientagdic)


    response = json.dumps(listclientctag,  ensure_ascii=False)
    print(response)
    return JsonResponse(response, safe=False) 




@login_required(login_url='/login')
def edit_tag(request, pk):
    #instancia
    tag = Tag.objects.get(pk=pk)
    # Creamos formulario con los datos de la instancia
    formtag = TagForm(request.POST, instance=tag)  #objeto
    #comprobamos formulario
    if request.method == 'POST':
        # Actualizamos el formulario con los datos recibidos
        formtag = TagForm(request.POST, instance=tag)
        # Si el formulario es válido
        if formtag.is_valid():
            formtag.save()
            tag = formtag.save(commit=False)
            tag.save()
            return redirect('home')
        else:
            formtag = CategoriaForm() #nuevo objeto
       

    return render(
        request=request,
        template_name='edit_categoria.html',
        context={
            
            'formtag': formtag,
            'tag': tag,
        })




# def delete_tag(request, pk):
#      #instancia
#     tag = Tag.objects.get(pk=pk)
#     if tag:
#         tag.delete()
#         return redirect('list_tag')


@login_required(login_url='/login')
def list_categoria(request):

    categorias = Categoria.objects.all()

    return render(
        request=request,
        template_name='list_categoria.html',
        context={
             'categorias': categorias,
          
        })



@login_required(login_url='/login')
def edit_categoria(request, pk):
    #instancia
    categoria = Categoria.objects.get(pk=pk)
    # Creamos formulario con los datos de la instancia
    form = CategoriaForm(request.POST, instance=categoria)  #objeto
    #comprobamos formulario
    if request.method == 'POST':
        # Actualizamos el formulario con los datos recibidos
        form = CategoriaForm(request.POST, instance=categoria)
        # Si el formulario es válido
        if form.is_valid():
            form.save()
            categoria = form.save(commit=False)
            categoria.save()
            return redirect('home')
        else:
            form = CategoriaForm() #nuevo objeto
       

    return render(
        request=request,
        template_name='edit_categoria.html',
        context={
            
            'form': form,
            'categoria': categoria,
        })


@login_required(login_url='/login')
def detail_categoria(request, pk):


   categorias = Categoria.objects.get(pk=pk)

   return render(
        request=request,
        template_name='detail_categoria.html',
        context={
            'categorias': categorias,
          
        })



@login_required(login_url='/login')
def add_dis(request, pk):
    if request.method == 'POST':
        formdis = DiscusionesForm(request.POST) 
        if formdis.is_valid():
            formdis.save()

  
            return HttpResponseRedirect(reverse('detail_proyecto', args=(pk,)))


# guardar discusion-proyecto mediante ajax
@login_required(login_url='/login')
@csrf_exempt
def ajax_save_dis(request, pk):
    data = request.body
    data = json.loads(data)

    asunto = (data['asunto'])
    descripcion = (data['descripcion'])
    usuario_id = (data['usuario'])  

    proyecto = Proyect.objects.get(pk=pk)#sera la relacion del pk de tarea 
    discusion = Discusiones.objects.create(asunto=asunto, usuario_id=usuario_id, proyecto=proyecto, descripcion=descripcion)
    response = JsonResponse({ 'success': 1, 'asunto': discusion.asunto, 'usuario': discusion.usuario.pk, 'proyecto': discusion.proyecto.pk, 'descripcion': discusion.descripcion,})
    return response 

    



@login_required(login_url='/login')
def list_dis(request):

    discusion = Discusiones.objects.all()

    return render(
        request=request,
        template_name='list_dis.html',
        context={
             'discusion': discusion,
          
        })




@login_required(login_url='/login')
def edit_dis(request, pk):
    #instancia
    discusion = Discusiones.objects.get(pk=pk)
    # Creamos formulario con los datos de la instancia
    formdis = DiscusionesForm(request.POST, instance=discusion)  #objeto
    #comprobamos formulario
    if request.method == 'POST':
        # Actualizamos el formulario con los datos recibidos
        formdis = DiscusionesForm(request.POST, instance=discusion)
        # Si el formulario es válido
        if formdis.is_valid():
            formdis.save()
            discusion = formdis.save(commit=False)
            discusion.save()
            return redirect('home')
    else:
        formdis = DiscusionesForm(instance=discusion) #nuevo objeto
       

    return render(
        request=request,
        template_name='edit_dis.html',
        context={
            
            'formdis': formdis,
            'discusion': discusion,
        })

# ajax editar discusion
@csrf_exempt
@login_required(login_url='/login')
def ajax_edit_dis(request, pk):
    data = request.body
    data = json.loads(data)
    asunto = (data['asunto'])
    descripcion = (data['descripcion'])
    Discusiones.objects.filter(pk=pk).update(asunto=asunto, descripcion=descripcion,)
    
    response = JsonResponse({ 'success' :1})
    return response


@login_required(login_url='/login')
def detail_dis(request, pk):


   discusion = Discusiones.objects.get(pk=pk)

   return render(
        request=request,
        template_name='detail_dis.html',
        context={
            'discusion': discusion,
          
        })


@login_required(login_url='/login')
def add_tarea(request, pk):
    if request.method == 'POST':
        formta = TareasForm(request.POST) 
        if formta.is_valid():
            formta.save()

  
            return HttpResponseRedirect(reverse('tarea_proyecto', args=(pk,)))

# guardar tarea-proyecto ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_save_tarea(request, pk):
    data = request.body
    data = json.loads(data)
    titulo = (data['titulo'])
    detalles = (data['detalles'])
    fechav = (data['fechav'])
    estado = (data['estado'])
    prioridad = (data['prioridad'])
    orden = (data['orden'])

    print('fecha ', fechav)
    proyecto = Proyect.objects.get(pk=pk)#sera la relacion del pk de tarea 

    newtask = Tareas.objects.create(owner = request.user, proyecto = proyecto, titulo=titulo, detalles=detalles, fechav=fechav, estado=estado, prioridad=prioridad, orden=orden,)
    response = JsonResponse({ 'success': 1,  'titulo': newtask.titulo,   'estado': newtask.estado,  'pk': newtask.pk,})

    return response 


@login_required(login_url='/login')
def list_tarea(request):


    tareas = Tareas.objects.all()

    return render(
        request=request,
        template_name='list_tarea.html',
        context={
             'tareas': tareas,
          
        })

@login_required(login_url='/login')
def edit_tarea(request, pk):
    #instancia
    tarea = Tareas.objects.get(pk=pk)
    # Creamos formulario con los datos de la instanci #objeto
    #comprobamos formulario
    if request.method == 'POST':
        # Actualizamos el formulario con los datos recibidos
        formta = TareasForm(request.POST, instance=tarea)
        # Si el formulario es válido
        if formta.is_valid():
            formta.save()
            tarea = form.save(commit=False)
            tarea.save()
            return redirect('home')
    else:
        formta = TareasForm(instance=tarea) #nuevo objeto
        

    return render(
        request=request,
        template_name='edit_tarea.html',
        context={
            
            'formta': formta,
            'tarea': tarea,
        })

# ajax editar tarea
@csrf_exempt
@login_required(login_url='/login')
def ajax_edit_tarea(request, pk):
    data = request.body
    data = json.loads(data)
    titulo = (data['titulo'])
    detalles = (data['detalles'])
    fechav = (data['fechav'])
    estado = (data['estado'])
    prioridad = (data['prioridad'])
    orden = (data['orden'])
    owner_id = (data['owner'])
    Tareas.objects.filter(pk=pk).update(titulo=titulo, detalles=detalles, fechav=fechav, estado=estado, prioridad=prioridad, orden=orden, owner_id=owner_id)
    
    response = JsonResponse({ 'success' :1})
    return response 



@login_required(login_url='/login')
def detail_tarea(request, pk):


    tarea = Tareas.objects.get(pk=pk)
    imagenes =  ImageTarea.objects.filter(tarea = tarea)
    comentario = ComentariosTarea.objects.filter(tarea = tarea)


    if request.method == 'POST':
        formimg = ImageTareaForm(request.POST) 
        if formimg.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('detail_tarea', args=(pk,)))
    else:
        #form = DiscusionesForm() #para crer un nuevo objeto
        formimg = ImageTareaForm(initial={'tarea': tarea,})
        formimg.fields['tarea'].widget = forms.HiddenInput()
        #form = comentareForm() #para crer un nuevo objeto
        formcom = ComentariosTareaForm(initial={'usuario': request.user, 'tarea': tarea,})
        formcom.fields['tarea'].widget = forms.HiddenInput()
        formcom.fields['usuario'].widget = forms.HiddenInput()


    return render(
        request=request,
        template_name='detail_tarea.html',
        context={
        'imagenes': imagenes,
        'formimg': formimg,
        'tarea': tarea,
        'comentario': comentario,
        'formcom': formcom,
        

          
    })


@login_required(login_url='/login')
def tarea_proyecto(request, pk):


    proyecto = Proyect.objects.get(pk=pk)

    tareas =  Tareas.objects.filter(proyecto = proyecto)


    if request.method == 'POST':
        formta = TareasForm(request.POST) 
        if formta.is_valid():
            formta.save()

            return HttpResponseRedirect(reverse('tarea_proyecto', args=(pk,)))
    else:
        #form = DiscusionesForm() #para crer un nuevo objeto
        formta = TareasForm(initial={'proyecto': proyecto, 'owner':request.user })
        formta.fields['proyecto'].widget = forms.HiddenInput()

        formta2 = TareasForm(initial={'proyecto': proyecto, 'usuario': request.user, 'owner':request.user})
        formta2.fields['proyecto'].widget = forms.HiddenInput()

        formcom2 = ComentariosTareaForm(initial={'usuario': request.user,})
        formcom2.fields['usuario'].widget = forms.HiddenInput()
        formcom2.fields['tarea'].widget = forms.HiddenInput()
        
        # # aqui va el form de imagen tarea
        # formimg2 = ImageTareaForm(initial={'usuario': request.user,})
        # formimg2.fields['tarea'].widget = forms.HiddenInput()
       
        


    return render(
        request=request,
        template_name='tareas_proyecto.html',
        context={
            'proyecto': proyecto, 
            'formcom2': formcom2, 
            'formta2': formta2, 
            'formta': formta, 
            # 'formimg2': formimg2,
            'tareas': tareas,
          
        })

@login_required(login_url='/login')
def get_tarea(request, pk):
    
    return render(
        request=request,
        template_name='tareas_detail.html',
    )


@login_required(login_url='/login')
@csrf_exempt
def tarea_detail(request, pk, estatus):
    data = request.body
    proyecto = Proyect.objects.get(pk=pk) #sera la relacion del pk de proyecto

    print(estatus)

    tareas = Tareas.objects.filter(proyecto = proyecto, estado = estatus)#sera la relacion de tareas con proyecto 

    listtarea =[]
    for  tare in  tareas:
        #crear un diccionario con los datos del producto, idproducto, nombre, precio y cantidad
        taredic = {
            'idtarea': tare.id, 
            'titulo': tare.titulo,
            'order': tare.orden,
            'estatus': tare.estado,
            #'subtotal':  produ.producto.price * produ.cantidad
            }

        #metodo append de agregar diccionario
        listtarea.append(taredic)


    response = json.dumps(listtarea,  ensure_ascii=False)
    print(response)
    return JsonResponse(response, safe=False)

# listar comentario tarea mediante ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_list_comentare(request, pk):
    tarea = Tareas.objects.get(pk=pk)#sera la relacion del pk de tarea
    comentarios = ComentariosTarea.objects.filter(tarea = tarea)

    print('entro vista ajax')
    listcomentare =[]
    for  tarcom in  comentarios:
        #crear un diccionario con los datos de comentariotarea, 
        comdic = {
            'descripcion': tarcom.descripcion, 
            'usuario': tarcom.usuario.first_name,
            'tarea': tarcom.tarea.pk,
            'fecha': str(tarcom.fecha),
            'idtarea': str(tarcom.id), 
            }

        #metodo append de agregar diccionario
        listcomentare.append(comdic)


    response = json.dumps(listcomentare,  ensure_ascii=False)
    print(response)
    return JsonResponse(response, safe=False) 




# listar photos tarea
@csrf_exempt
@login_required(login_url='/login')
def ajax_list_photos(request, pk):
    tarea = Tareas.objects.get(pk=pk)#sera la relacion del pk de tarea
    photos = ImageTarea.objects.filter(tarea = tarea)

    print('entro vista ajax')
    listcomentare =[]
    for  photo in  photos:
        #crear un diccionario con los datos de comentariotarea, 
        comdic = {'is_valid': True, 'tarea' : photo.tarea.pk, 'name': photo.file.name, 'url': photo.file.url}

        #metodo append de agregar diccionario
        listcomentare.append(comdic)


    response = json.dumps(listcomentare,  ensure_ascii=False)
    print(response)
    return JsonResponse(response, safe=False) 


# listar comentario tarea mediante ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_list_campa(request, pk):
    publicidad = ProyectoPublicidad.objects.get(pk=pk)
    campana = HistorialCampana.objects.filter(proyecto = publicidad)

    print('entro vista ajax')
    listcampa =[]
    for  campa in  campana:
        #crear un diccionario con los datos de comentariotarea, 
        camdic = {
            'fechaini': str(campa.fechaini), 
            'fechater': str(campa.fechater),
            'inversion': campa.inversion,
            'proyecto': campa.proyecto.pk,
            'comentario': campa.comentario,
            'comision': campa.comision,   
            'idcam': str(campa.id), 
            }

        #metodo append de agregar diccionario
        listcampa.append(camdic)


    response = json.dumps(listcampa,  ensure_ascii=False)
    print(response)
    return JsonResponse(response, safe=False) 

    

# guardar comentario-tarea mediante ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_save_comentarea(request, pk):
    data = request.body
    data = json.loads(data)

    descripcion = (data['descripcion'])
    usuario_id = (data['usuario'])
    fecha = (data['fecha'])

    tarea = Tareas.objects.get(pk=pk)#sera la relacion del pk de tarea 
    comentario = ComentariosTarea.objects.create(descripcion=descripcion, usuario_id=usuario_id, tarea=tarea, fecha=fecha)
    response = JsonResponse({ 'success': 1, 'descripcion': comentario.descripcion, 'usuario': comentario.usuario.pk, 'tarea': comentario.tarea.pk, 'fecha': comentario.fecha,})
    return response 

# # guardar comentario-tarea mediante ajax
# @csrf_exempt
# def ajax_save_comentarea(request, pk):
#     data = request.body
#     data = json.loads(data)

#     descripcion = (data['descripcion'])
#     usuario_id = (data['usuario'])
#     fecha = (data['fecha'])

#     tarea = Tareas.objects.get(pk=pk)#sera la relacion del pk de tarea
#     ComentariosTarea.objects.create(descripcion=descripcion, usuario_id=usuario_id, tarea=tarea, fecha=fecha)
    
#     response = JsonResponse({ 'success': 1,})
#     return response 


@login_required(login_url='/login')
def add_comentare(request, pk):
    if request.method == 'POST':
        print(request.POST)
        print('entro post comentare')
        formcom = ComentariosTareaForm(request.POST) 
        if formcom.is_valid():
            print('form valid')
            formcom.save()

  
            return HttpResponseRedirect(reverse('detail_tarea', args=(pk,)))


   
   
@login_required(login_url='/login')
def list_comentare(request):

    comentario = ComentariosTarea.objects.all()

    return render(
        request=request,
        template_name='list_comentare.html',
        context={
             'comentario': comentario,
          
        })


@login_required(login_url='/login')
def edit_comentare(request, pk):
    #instancia
    comentario = ComentariosTarea.objects.get(pk=pk)

    if request.method == 'POST':
        # Actualizamos el formulario con los datos recibidos
        form = ComentariosTareaForm(request.POST, instance=comentario)
        # Si el formulario es válido
        if form.is_valid():
            form.save()
            comentario = form.save(commit=False)
            comentario.save()
            return redirect('home')
    else:
        form = ComentariosTareaForm(instance=comentario) #nuevo objeto

       

    return render(
        request=request,
        template_name='edit_comentare.html',
        context={
            
            'form': form,
            'comentario': comentario,
        })


@csrf_exempt
@login_required(login_url='/login')
def ajax_edit_comentare(request, pk):
    data = request.body
    data = json.loads(data)
    descripcion = (data['descripcion'])
    usuario_id = (data['usuario'])
    fecha = (data['fecha'])
    ComentariosTarea.objects.filter(pk=pk).update(descripcion=descripcion, usuario_id=usuario_id, fecha=fecha)
    
    response = JsonResponse({ 'success' :1})
    return response  


@login_required(login_url='/login')
def detail_comentare(request, pk):

    tarea = Tareas.objects.get(pk=pk)
    comentarios = ComentariosTarea.objects.filter(tarea = tarea)
   # print('comentario')
   # print(comentario.pk)


    if request.method == 'POST':
        form = ComentariosTareaForm(request.POST) 
        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('detail_comentare', args=(pk,)))
    else:
        #form = DiscusionesForm() #para crer un nuevo objeto
        form = ComentariosTareaForm(initial={'usuario': request.user, 'tarea': tarea,})
        form.fields['usuario'].widget = forms.HiddenInput()
        form.fields['tarea'].widget = forms.HiddenInput()


    return render(
        request=request,
        template_name='detail_comentare.html',
        context={
            'comentarios': comentarios,
            'form': form,
            'tarea': tarea
          
        })


@login_required(login_url='/login')
def add_imagetarea(request, pk):
    if request.method == 'POST':
        formimg = ImageTareaForm(request.POST) 
        if formimg.is_valid():
            formimg.save()

  
            return HttpResponseRedirect(reverse('detail_tarea', args=(pk,)))

    
@login_required(login_url='/login')
def list_imagetarea(request):

    imagenes = ImageTarea.objects.all()

    return render(
        request=request,
        template_name='list_imagetarea.html',
        context={
             'imagenes': imagenes,
          
        })


@login_required(login_url='/login')
def add_servicio(request, pk):
    proyecto = Proyect.objects.get(pk=pk)
    formser = ServiciosExtrasForm(initial={'proyecto': proyecto})
    formser.fields['proyecto'].widget = forms.HiddenInput()
    if request.method == 'POST': 
        formser = ServiciosExtrasForm(request.POST)
        if formser.is_valid():
            formser.save()

  
            return HttpResponseRedirect(reverse('detail_proyecto', args=(pk,)))

# guardar servicio-proyecto mediante ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_save_servicio(request, pk):
    data = request.body
    data = json.loads(data)

    titulo = (data['titulo'])
    descripcion = (data['descripcion']) 
    costo = (data['costo'])  
    tipo = (data['tipo'])  

    proyecto = Proyect.objects.get(pk=pk)#sera la relacion del pk de tarea 
    servicio = ServiciosExtras.objects.create(titulo=titulo, descripcion=descripcion, proyecto=proyecto, costo=costo, tipo=tipo)
    response = JsonResponse({ 'success': 1, 'titulo': servicio.titulo, 'descripcion': servicio.descripcion, 'proyecto': servicio.proyecto.pk, 'costo': servicio.costo, 'idservicio': servicio.pk,})
    return response 

# actualizar_servicio-proyecto ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_save_fill_servicio(request, pk):
    data = request.body
    data = json.loads(data)
    titulo = (data['titulo'])
    descripcion = (data['descripcion']) 
    costo = (data['costo']) 


    proyecto = Proyect.objects.get(pk=pk)#sera la relacion del pk de tarea 
    ServiciosExtras.objects.filter(pk=proyecto.pk).update(titulo=titulo, descripcion=descripcion, costo=costo,)
    response = JsonResponse({ 'success': 1,})

    return response

# rellenar forumulario
@csrf_exempt
@login_required(login_url='/login')
def ajax_fill_servicio(request, pk):
    servicio = ServiciosExtras.objects.get(pk=pk)
    response = JsonResponse({ 'success': 1, 'titulo':servicio.titulo, 'descripcion': servicio.descripcion, 'fecha': servicio.fecha, 'costo':servicio.costo, 'proyecto':servicio.proyecto.pk})

    return response
        
      


@login_required(login_url='/login')
def list_servicio(request):
    
    servicios = ServiciosExtras.objects.all()

    return render(
        request=request,
        template_name='list_servicio.html',
        context={
             'servicios': servicios,
          
        })


@login_required(login_url='/login')
def edit_servicio(request, pk):
    #instancia
    servicio = ServiciosExtras.objects.get(pk=pk)
    # Creamos formulario con los datos de la instancia
    formser = ServiciosExtrasForm(request.POST,)  #objeto
    #comprobamos formulario
    if request.method == 'POST':
        # Actualizamos el formulario con los datos recibidos
        formser = ProyectForm(request.POST,)
        # Si el formulario es válido
        if formser.is_valid():
            formser.save()
            servicio = formser.save(commit=False)
            servicio.save()
            return redirect('home')
    else:
        formser = ServiciosExtrasForm() #nuevo objeto
       

    return render(
        request=request,
        template_name='edit_servicio.html',
        context={
            
            'formser': formser,
            'servicio': servicio,
        })


 
@login_required(login_url='/login')
def detail_servicio(request, pk):


    proyecto = Proyect.objects.get(pk=pk)

    servicios =  ServiciosExtras.objects.filter(proyecto = proyecto)


    if request.method == 'POST':
        form = ServiciosExtrasForm(request.POST) 
        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('detail_servicio', args=(pk,)))
    else:
        #form = DiscusionesForm() #para crer un nuevo objeto
        form = ServiciosExtrasForm(initial={'proyecto': proyecto,})
        form.fields['proyecto'].widget = forms.HiddenInput()


    return render(
        request=request,
        template_name='detail_servicio.html',
        context={
            'proyecto': proyecto, 'form': form, 'servicios': servicios,
          
        })

@login_required(login_url='/login')
def add_pagopro(request, pk):
    proyecto = Proyect.objects.get(pk=pk)
    formpa = PagosProyectoForm(initial={'proyecto': proyecto})
    formpa.fields['proyecto'].widget = forms.HiddenInput()
    if request.method == 'POST': 
        formpa = PagosProyectoForm(request.POST)
        if formpa.is_valid():
            formpa.save()

  
            return HttpResponseRedirect(reverse('detail_proyecto', args=(pk,)))

# guardar pago-proyecto mediante ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_save_pagopro(request, pk):
    data = request.body
    data = json.loads(data)
    monto = (data['monto']) 
    comentario = (data['comentario']) 
    estatus = (data['estatus']) 
    proyecto = (data['proyecto'])  

    proyecto = Proyect.objects.get(pk=pk)#sera la relacion del pk de tarea 
    pago = PagosProyecto.objects.create( monto=monto, comentario=comentario, estatus=estatus, proyecto=proyecto)
    response = JsonResponse({ 'success': 1, 'monto': pago.monto, 'proyecto': pago.proyecto.pk, 'estatus': pago.estatus, 'comentario': pago.comentario, 'idpagopro': pago.pk})
    return response 

# actualizar_servicio-proyecto ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_save_fill_pagopro(request, pk):
    data = request.body
    data = json.loads(data)

    print(data)
    print('pk')
    print(pk)
  
    monto = (data['monto']) 
    comentario = (data['comentario']) 
    estatus = (data['estatus']) 

    PagosProyecto.objects.filter(pk=pk).update(monto=monto, comentario=comentario, estatus=estatus,)
    response = JsonResponse({ 'success': 1,})
    print('daved')

    return response

# rellenar forumulario
@csrf_exempt
@login_required(login_url='/login')
def ajax_fill_pagopro(request, pk):
    pago = PagosProyecto.objects.get(pk=pk)
    response = JsonResponse({ 'success': 1, 'fecha':pago.fecha, 'monto': pago.monto, 'comentario': pago.comentario, 'estatus':pago.estatus, 'proyecto':pago.proyecto.pk})

    return response
        
      

    
@login_required(login_url='/login')
def list_pagopro(request):

    pagos = PagosProyecto.objects.all()

    return render(
        request=request,
        template_name='list_pagopro.html',
        context={
             'pagos': pagos,
          
        })


@login_required(login_url='/login')
def edit_pagopro(request, pk):
    #instancia
    pago = PagosProyecto.objects.get(pk=pk)
    # Creamos formulario con los datos de la instancia
    formpa = PagosProyectoForm(request.POST,)  #objeto
    #comprobamos formulario
    if request.method == 'POST':
        # Actualizamos el formulario con los datos recibidos
        formpa = PagosProyectoForm(request.POST,)
        # Si el formulario es válido
        if formpa.is_valid():
            formpa.save()
            pago = formpa.save(commit=False)
            pago.save()
            return redirect('home')
    else:
        formpa = PagosProyectoForm() #nuevo objeto
        formpa.fields['proyecto'].widget = forms.HiddenInput()
       

    return render(
        request=request,
        template_name='edit_pagopro.html',
        context={
            
            'formpa': formpa,
            'pago': pago,
        })

@csrf_exempt
@login_required(login_url='/login')
def ajax_edit_pagopro(request, pk):
    data = request.body
    data = json.loads(data)
    monto = (data['monto']) 
    comentario = (data['comentario']) 
    estatus = (data['estatus'])   
    PagosProyecto.objects.filter(pk=pk).update(monto=monto, comentario=comentario, estatus=estatus)
    
    response = JsonResponse({ 'success' :1})
    return response  



@login_required(login_url='/login')
def detail_pagopro(request, pk):


    proyecto = Proyect.objects.get(pk=pk)

    pagos =  PagosProyecto.objects.filter(proyecto = proyecto)


    if request.method == 'POST':
        form = PagosProyectoForm(request.POST) 
        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('detail_pagopro', args=(pk,)))
    else:
        #form = DiscusionesForm() #para crer un nuevo objeto
        form = PagosProyectoForm(initial={'proyecto': proyecto,})
        form.fields['proyecto'].widget = forms.HiddenInput()


    return render(
        request=request,
        template_name='detail_pagopro.html',
        context={
            'proyecto': proyecto, 'form': form, 'pagos': pagos,
          
        })


@login_required(login_url='/login')
def add_publi(request, pk):
    proyecto = Proyect.objects.get(pk=pk)
    formpu = ProyectoPublicidadForm(initial={'proyecto': proyecto})
    formpu.fields['proyecto'].widget = forms.HiddenInput()
    if request.method == 'POST': 
        formpu = ProyectoPublicidadForm(request.POST)
        if formpu.is_valid():
            formpu.save()

  
            return HttpResponseRedirect(reverse('detail_proyecto', args=(pk,)))

# guardar publicidad-proyecto mediante ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_save_publi(request, pk):
    data = request.body
    data = json.loads(data)

    estado = (data['estado'])
    nombre = (data['nombre']) 
    empresa = (data['empresa']) 
    idpublicidad = (data['idpublicidad'])   
    promocion = (data['promocion'])
    proyecto = (data['proyecto']) 

    proyecto = Proyect.objects.get(pk=pk)#sera la relacion del pk de tarea 
    publi = ProyectoPublicidad.objects.create(estado=estado, nombre=nombre, empresa=empresa, proyecto=proyecto, idpublicidad=idpublicidad, promocion=promocion)
    response = JsonResponse({ 'success': 1, 'estado': publi.estado, 'nombre': publi.nombre, 'proyecto': publi.proyecto.pk, 'empresa': publi.empresa, 'idpublicidad': publi.idpublicidad, 'promocion': publi.promocion, 'idpubli': publi.pk})
    return response 

# actualizar_publi-proyecto ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_save_fill_publi(request, pk):
    data = request.body
    data = json.loads(data)
    estado = (data['estado'])
    nombre = (data['nombre']) 
    empresa = (data['empresa']) 
    idpublicidad = (data['idpublicidad'])   
    promocion = (data['promocion']) 

    proyecto = Proyect.objects.get(pk=pk)#sera la relacion del pk de tarea 
    ProyectoPublicidad.objects.filter(pk=proyecto.pk).update(estado=estado, nombre=nombre, empresa=empresa, idpublicidad=idpublicidad, promocion=promocion,)
    response = JsonResponse({ 'success': 1,})

    return response

# rellenar forumulario
@csrf_exempt
@login_required(login_url='/login')
def ajax_fill_publi(request, pk):
    publicidad = ProyectoPublicidad.objects.get(pk=pk)
    response = JsonResponse({ 'success': 1, 'promocion':publicidad.promocion, 'estado':publicidad.estado, 'nombre': publicidad.nombre, 'empresa': publicidad.empresa, 'idpublicidad':publicidad.idpublicidad, 'proyecto':publicidad.proyecto.pk})

    return response       
      

    


@login_required(login_url='/login')
def list_publi(request):

    publicidad = ProyectoPublicidad.objects.all()

    return render(
        request=request,
        template_name='list_publi.html',
        context={
             'publicidad': publicidad,
          
        })


@login_required(login_url='/login')
def edit_publi(request, pk):
    #instancia
    publicidad = ProyectoPublicidad.objects.get(pk=pk)
    # Creamos formulario con los datos de la instancia
    formpu = ProyectoPublicidadForm(request.POST,)  #objeto
    #comprobamos formulario
    if request.method == 'POST':
        # Actualizamos el formulario con los datos recibidos
        formpu = ProyectoPublicidadForm(request.POST,)
        # Si el formulario es válido
        if formpu.is_valid():
            formpu.save()
            publicidad = formpu.save(commit=False)
            publicidad.save()
            return redirect('home')
    else:
        formpu = ProyectoPublicidadForm() #nuevo objeto
       

    return render(
        request=request,
        template_name='edit_publi.html',
        context={
            
            'formpu': formpu,
            'publicidad': publicidad,
        })

# ajax editar publi
@csrf_exempt
@login_required(login_url='/login')
def ajax_edit_publi(request, pk):
    data = request.body
    data = json.loads(data)
    estado = (data['estado'])
    nombre = (data['nombre']) 
    empresa = (data['empresa']) 
    idpublicidad = (data['idpublicidad'])   
    promocion = (data['promocion'])
    ProyectoPublicidad.objects.filter(pk=pk).update(estado=estado, nombre=nombre, empresa=empresa, idpublicidad=idpublicidad, promocion=promocion)
    
    response = JsonResponse({ 'success' :1})
    return response




@login_required(login_url='/login')
def detail_publi(request, pk):


    proyecto = Proyect.objects.get(pk=pk)

    publicidad =  ProyectoPublicidad.objects.filter(proyecto = proyecto)


    if request.method == 'POST':
        form = ProyectoPublicidadForm(request.POST) 
        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('detail_publi', args=(pk,)))
    else:
        #form = ProyectoPublicidadForm() #para crer un nuevo objeto
        form = ProyectoPublicidadForm(initial={'proyecto': proyecto,})
        form.fields['proyecto'].widget = forms.HiddenInput()


    return render(
        request=request,
        template_name='detail_publi.html',
        context={
            'proyecto': proyecto, 'form': form, 'publicidad': publicidad
          
        })



@login_required(login_url='/login')
def detail_proyectopubli(request, pk):

    publicidad = ProyectoPublicidad.objects.get(pk=pk)
    campana = HistorialCampana.objects.filter(proyecto = publicidad)
    
    #form = historialcampa() #para crer un nuevo objeto
    formca = HistorialCampanaForm(initial={'proyecto': publicidad,})
    formca.fields['proyecto'].widget = forms.HiddenInput()
    #form = DiscusionesForm() #para crer un nuevo objeto
    


    return render(
        request=request,
        template_name='detail_proyectopubli.html',
        context={
            'publicidad': publicidad, 
            'formca': formca,
            'campana': campana,

          
        })




@login_required(login_url='/login')
def add_campa(request, pk):
    proyecto = Proyect.objects.get(pk=pk)
    formca = HistorialCampanaForm(initial={'proyecto': proyecto})
    formca.fields['proyecto'].widget = forms.HiddenInput()
    if request.method == 'POST': 
        formca = HistorialCampanaForm(request.POST)
        if formca.is_valid():
            formca.save()

  
            return HttpResponseRedirect(reverse('detail_proyecto', args=(pk,)))


# guardar campa-proyecto mediante ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_save_campa(request, pk):
    data = request.body
    data = json.loads(data)

    fechaini = (data['fechaini'])
    fechater = (data['fechater']) 
    inversion = (data['inversion']) 
    comision = (data['comision'])   
    comentario = (data['comentario'])
    proyecto = (data['proyecto']) 

    proyecto = ProyectoPublicidad.objects.get(pk=pk)#sera la relacion del pk de tarea 
    campa = HistorialCampana.objects.create(fechaini=fechaini, fechater=fechater, inversion=inversion, proyecto=proyecto, comision=comision, comentario=comentario)
    response = JsonResponse({ 'success': 1, 'fechaini': campa.fechaini, 'fechater': campa.fechater, 'proyecto': campa.proyecto.pk,  'inversion': campa.inversion, 'comision': campa.comision, 'comentario': campa.comentario, 'idcampa': campa.pk})
    return response 


# actualizar_campa-proyecto ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_save_fill_campa(request, pk):
    data = request.body
    data = json.loads(data)
    fechaini = (data['fechaini'])
    fechater = (data['fechater']) 
    inversion = (data['inversion']) 
    comision = (data['comision'])   
    comentario = (data['comentario'])

    proyecto = ProyectoPublicidad.objects.get(pk=pk)#sera la relacion del pk de tarea 
    HistorialCampana.objects.filter(pk=proyecto.pk).update(fechaini=fechaini, fechater=fechater, inversion=inversion, comision=comision, comentario=comentario)
    response = JsonResponse({ 'success': 1,})

    return response

# rellenar forumulario
@csrf_exempt
@login_required(login_url='/login')
def ajax_fill_campa(request, pk):
    campana = HistorialCampana.objects.get(pk=pk)
    response = JsonResponse({ 'success': 1, 'fechaini':campana.fechaini, 'fechater':campana.fechater, 'inversion': campana.inversion, 'comision': campana.comision, 'comentario':campana.comentario, 'proyecto':campana.proyecto.pk})

    return response       
                      
      

    
@login_required(login_url='/login')
def list_campa(request):

    campana = HistorialCampana.objects.all()

    return render(
        request=request,
        template_name='list_campa.html',
        context={
             'campana': campana,
          
        })


@login_required(login_url='/login')
def edit_campa(request, pk):
    #instancia
    campana = HistorialCampana.objects.get(pk=pk)
    # Creamos formulario con los datos de la instancia
    formca = HistorialCampanaForm(request.POST,)  #objeto
    #comprobamos formulario
    if request.method == 'POST':
        # Actualizamos el formulario con los datos recibidos
        formca = HistorialCampanaForm(request.POST,)
        # Si el formulario es válido
        if formca.is_valid():
            formca.save()
            campana = formca.save(commit=False)
            campana.save()
            return redirect('home')
    else:
        formca = HistorialCampanaForm() #nuevo objeto
       

    return render(
        request=request,
        template_name='edit_campa.html',
        context={
            
            'formca': formca,
            'campana': campana,
        })

# ajax editar publi
@csrf_exempt
@login_required(login_url='/login')
def ajax_edit_campa(request, pk):
    data = request.body
    data = json.loads(data)
    fechaini = (data['fechaini'])
    fechater = (data['fechater']) 
    inversion = (data['inversion']) 
    comision = (data['comision'])   
    comentario = (data['comentario'])
    HistorialCampana.objects.filter(pk=pk).update(fechaini=fechaini, fechater=fechater, inversion=inversion, comision=comision, comentario=comentario)
    
    response = JsonResponse({ 'success' :1})
    return response


#ajax status


@csrf_exempt
@login_required(login_url='/login')
def ajax_change_proyect_status(request, pk):
    data = request.body
    data = json.loads(data)
    estatus_id = (data['estatus_id'])
    Proyect.objects.filter(pk=pk).update(estado=estatus_id)
    
    response = JsonResponse({ 'success' :1})
    return response

@login_required(login_url='/login')
def detail_campa(request, pk):


    proyecto = Proyect.objects.get(pk=pk)

    campana =  HistorialCampana.objects.filter(proyecto = proyecto)


    if request.method == 'POST':
        form = HistorialCampanaForm(request.POST) 
        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('detail_campa', args=(pk,)))
    else:
        #form = ProyectoPublicidadForm() #para crer un nuevo objeto
        form = ProyectoPublicidadForm(initial={'proyecto': proyecto,})
        form.fields['proyecto'].widget = forms.HiddenInput()


    return render(
        request=request,
        template_name='detail_campa.html',
        context={
            'proyecto': proyecto, 'form': form, 'campana': campana
          
        })



class homeview(TemplateView):
    template_name='homeview.html'

class search(ListView): 
    model = Proyect
    template_name = 'search.html'
    

# funcion buscador de proyectos 
    def get_queryset(self):
        proyecto = self.request.GET.get('q')
        print('query', proyecto)
        listpro = Proyect.objects.filter(
            Q(nombre__icontains=proyecto) | Q(detalles__icontains=proyecto)
        )


        print('listpro, ', listpro)
     

        return listpro

# exportar a excel
@login_required(login_url='/login')
def export_proyect(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="proyect.csv"'
    writer = csv.writer(response)
    writer.writerow(['Nombre', 'Categoria', 'Detalles', 'Usuario'])
    proyectos = Proyect.objects.all().values_list('nombre', 'categoria', 'detalles', 'usuario')
    for proyect in proyectos:
        writer.writerow(proyect)    
    return response

@login_required(login_url='/login')
def export_tarea(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="tareas.csv"'
    writer = csv.writer(response)
    writer.writerow(['Titulo', 'Detalles', 'Fechav', 'Estado', 'Usuario', 'Proyecto'])
    tarea = Tareas.objects.all().values_list('titulo', 'detalles', 'fechav', 'estado', 'usuario', 'proyecto', )
    for tareas in tarea:
        writer.writerow(tareas)    
    return response

@login_required(login_url='/login')
def export_pagopro(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="pagosproyecto.csv"'
    writer = csv.writer(response)
    writer.writerow(['Fecha', 'Monto', 'Comentario', 'Proyecto'])
    pagopro = PagosProyecto.objects.all().values_list('fecha', 'monto', 'comentario', 'proyecto', )
    for pagosproyecto in pagopro:
        writer.writerow(pagosproyecto)    
    return response 


# guardar update_tarea-proyecto ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_fill_tarea(request, pk):
    tarea = Tareas.objects.get(pk=pk)
    response = JsonResponse({ 'success': 1, 'titulo':tarea.titulo, 'detalles': tarea.detalles, 'fechav': tarea.fechav, 'fechac':tarea.fechac, 'estado':tarea.estado, 'prioridad':tarea.prioridad, 'orden': tarea.orden,})

    return response 

@csrf_exempt
@login_required(login_url='/login')
def fill_tarea_imagen(request, pk):
    tarea = Tareas.objects.get(pk=pk)
    response = JsonResponse({ 'success': 1, 'titulo':tarea.titulo, 'detalles': tarea.detalles, 'fechav': tarea.fechav, 'fechac':tarea.fechac, 'estado':tarea.estado, 'prioridad':tarea.prioridad, 'orden': tarea.orden,})

    return response 



# actualizar_tarea-proyecto ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_save_fill_tarea(request, pk):
    data = request.body
    data = json.loads(data)
    titulo = (data['titulo'])
    detalles = (data['detalles'])
    fechav = (data['fechav'])
    estado = (data['estado'])
    prioridad = (data['prioridad'])
    orden = (data['orden'])


    tarea = Tareas.objects.get(pk=pk)#sera la relacion del pk de tarea 
    Tareas.objects.filter(pk=tarea.pk).update(titulo=titulo, detalles=detalles, fechav=fechav, estado=estado, prioridad=prioridad, orden=orden,)
    response = JsonResponse({ 'success': 1,})

    return response 




# actualizar_tarea-proyecto ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_set_img_pk(request, pk):
    data = request.body
    data = json.loads(data)

    print('data')
    print(data)

    imgpk = (data['imgpk'])

    tarea = Tareas.objects.get(pk=pk)#sera la relacion del pk de tarea 
    ImageTarea.objects.filter(pk=imgpk).update(tarea=tarea)
    response = JsonResponse({ 'success': 1,})

    return response 




# guardar_comentario-proyecto ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_save_cometario(request):
    data = request.body
    data = json.loads(data)

    descripcion = (data['descripcion'])
    idtarea = (data['idtarea'])

    tarea = Tareas.objects.get(pk=idtarea)#sera la relacion del pk de tarea 
    usuario = request.user
    coment = ComentariosTarea.objects.create(descripcion=descripcion, usuario=usuario, tarea=tarea)
    response = JsonResponse({ 'success': 1,  'descripcion': coment.descripcion, 'usuario': coment.usuario.first_name, 'fecha':coment.fecha })
    return response 


from django.utils.decorators import method_decorator
class BasicUploadView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(BasicUploadView, self).dispatch(*args, **kwargs)

    def get(self, request):
        photos_list = ImageTarea.objects.all()
        return render(self.request, 'photho.html', {'photos': photos_list})

    def post(self, request):
        formphoto = ImageTareaForm(self.request.POST, self.request.FILES)
        
        print('enter ajax')
        print(self.request.POST)
        if formphoto.is_valid():
            print('validad ajax')
            photo = formphoto.save()
            data = {'dato':self.request.POST,'is_valid': True, 'tarea' : photo.tarea.pk, 'name': photo.file.name, 'url': photo.file.url, 'imgpk': photo.pk}
        else:
            data = {'dato':self.request.POST,'is_valid': False}
        return JsonResponse(data)





             

