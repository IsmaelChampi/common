from django import template

from proyectos.models import Tareas
register = template.Library()



@register.simple_tag
def avancetag(proyectos):
	try:
		tareas_pendiente = Tareas.objects.filter(proyecto = proyectos, estado = 1).count()
		tareas_proceso = Tareas.objects.filter(proyecto = proyectos, estado = 2).count()
		tareas_completado = Tareas.objects.filter(proyecto = proyectos, estado = 3).count()
		total_tareas = tareas_pendiente+tareas_proceso+tareas_completado
		avance = (tareas_completado * 100) / total_tareas
	except:
		avance = 'None'
	return avance