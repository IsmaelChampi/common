from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.urls import path


# View
from clientes import views 


urlpatterns = [

    # Clientes
    path('add_client', views.add_client, name='add_client'),
    path('list_client', views.list_client, name='list_client'),


    path('edit_client/<int:pk>/', views.edit_client, name='edit_client'),
    path('detail_client/<int:pk>/', views.detail_client, name='detail_client'),
    path('delete_client/<int:pk>/', views.delete_client, name='delete_client'),
    path('ajax_save_cliente/<int:pk>/', views.ajax_save_cliente, name='ajax_save_cliente'),
    path('ajax_list_cliente/<int:pk>/', views.ajax_list_cliente, name='ajax_list_cliente'),
    path('ajax_edit_cliente/<int:pk>/', views.ajax_edit_cliente, name='ajax_edit_cliente'),
    path('ajax_save_fill_client/<int:pk>/', views.ajax_save_fill_client, name='ajax_save_fill_client'),
    path('ajax_fill_client/<int:pk>/', views.ajax_fill_client, name='ajax_fill_client'),

    path('searchc', views.search_clients.as_view(), name='searchc'), 



    # Ajax
    path('filtros/estados/', views.filtros_estados, name='filtros_estados'),
    path('filtros/municipios/<int:pk>/', views.filtros_municipios, name='filtros_municipios'),

    

    

      
]