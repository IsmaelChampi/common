from django.db import models
from django.utils.translation import ugettext as _
#from proyectos.models import Tag

# Create your models here.

from common.models import Perfil
from django.db.models.signals import post_save
from django.dispatch import receiver

import json
import os
from django.shortcuts import get_object_or_404
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

class Estado(models.Model):
    """ Modelo almacenar estado """
    idestado = models.IntegerField(verbose_name=_('ID unico'), blank=True, null=True)
    estado = models.CharField(verbose_name=_('Nombre de estado'), max_length=100)

    def __str__(self):
        return '%s' % self.estado

    class Meta:
        verbose_name = _('Estado')
        verbose_name_plural = _('Estados')
        default_permissions = ('add', 'change', 'delete', 'view')


class Municipio(models.Model):
    """ Modelo almacenar municipio """
    idmunicipio = models.IntegerField(verbose_name=_('ID unico'), blank=True, null=True)
    estado = models.ForeignKey(Estado, verbose_name=_('Estado'), on_delete=models.CASCADE)
    municipio = models.CharField(verbose_name=_('Nombre de municipio'), max_length=100)

    def __str__(self):
        return '%s' % self.municipio

    class Meta:
        verbose_name = _('Municipio')
        verbose_name_plural = _('Municipios')
        default_permissions = ('add', 'change', 'delete', 'view')


class Client(models.Model):
    """ Modelo de cliente """
    empresa = models.CharField(max_length=200, blank=True)
    email_Corporativo = models.CharField(max_length=100, blank=True)
    direccion_empresa = models.CharField(max_length=400, blank=True)

    pais_options = (
        (1, 'México'),
        (2, 'Estados Unidos'),
        (3, 'Colombia'),
        (4, 'Peru'),
        (5, 'Chile'),
        (5, 'España'),
        (5, 'Argentina'),
    )
    pais = models.IntegerField(verbose_name=_('País'), choices=pais_options)
    estado = models.ForeignKey(Estado, verbose_name=_('Estado'), blank=True, null=True,on_delete=models.CASCADE )
    municipio = models.ForeignKey(Municipio, verbose_name=_('Municipio'), blank=True, null=True, on_delete=models.CASCADE)  # poner chained field

    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)
    email_Personal = models.CharField(max_length=100, blank=True)
    telefono = models.CharField(max_length=15, blank=True)
    celular = models.CharField(max_length=15, blank=True)
    date = models.DateField(blank=True, null=True)

    nombre2 = models.CharField(max_length=200, blank=True)
    apellido2 = models.CharField(max_length=200, blank=True)
    email_Personal2 = models.CharField(max_length=100,  blank=True)
    telefono2 = models.CharField(max_length=15, blank=True)
    celular2 = models.CharField(max_length=15, blank=True)
    date2 = models.DateField(blank=True, null=True)

    comentarios = models.TextField(max_length=500, blank=True)
    dominio = models.CharField(max_length=100, blank=True)

    rfc = models.CharField(max_length=100,blank=True)
    razonsocial = models.CharField(max_length=300, blank=True)
    direccion = models.CharField(max_length=400, blank=True)
    correo = models.CharField(max_length=200, blank=True)
    gasto_options = (
        (1, '[D01] - Honorarios médicos, dentales y gastos hospitalarios.'),
        (2, '[D02] - Gastos médicos por incapacidad o discapacidad'),
        (3, '[D03] - Gastos funerales.'),
        (4, '[D04] - Donativos.'),
        (5, '[D05] - Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).'),
        (6, '[D06] - Aportaciones voluntarias al SAR.'),
        (7, '[D07] - Primas por seguros de gastos médicos.'),
        (8, '[D08] - Gastos de transportación escolar obligatoria.'),
        (9, '[D09] - Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.'),
        (10, '[D10] - Pagos por servicios educativos (colegiaturas)'),
        (11, '[G01] - Adquisición de mercancias'),
        (12, '[G02] - Devoluciones, descuentos o bonificaciones'),
        (13, '[G03] - Gastos en general'),
        (14, '[I01] - Construcciones'),
        (15, '[I02] - Mobilario y equipo de oficina por inversiones'),
        (16, '[I03] - Equipo de transporte'),
        (17, '[I04] - Equipo de computo y accesorios'),
        (18, '[I05] - Dados, troqueles, moldes, matrices y herramental'),
        (19, '[I06] - Comunicaciones telefónicas'),
        (20, '[I07] - Comunicaciones satelitales'),
        (21, '[I08] - Otra maquinaria y equipo'),
        (22, '[P01] - Por definir')
    )
    uso_cfdi = models.IntegerField(verbose_name=_('Uso del comprobante'), choices=gasto_options)
    comentario_factura = models.TextField(max_length=1000, blank=True)



    rfc_2 = models.CharField(max_length=100,blank=True)
    razonsocial_2 = models.CharField(max_length=300, blank=True)
    direccion_2 = models.CharField(max_length=400, blank=True)
    correo_2 = models.CharField(max_length=200, blank=True)
    uso_cfdi_2 = models.IntegerField(verbose_name=_('Uso del comprobante'), choices=gasto_options)
    comentario_factura_2 = models.TextField(max_length=1000, blank=True)


    origen_options = (
        (1, 'Google'),
        (2, 'Facebook'),
        (3, 'Yahoo'),
        (4, 'Recomendado'),
        (5, 'Otro'),
    )
    origen = models.IntegerField(verbose_name=_('Origen'), choices=origen_options)
    marca_options = (
        (1, 'Novemp'),
        (2, 'New'),
        (3, 'Youp'),
        (4, 'Markon'),
    )
    marca = models.IntegerField(verbose_name=_('Origen'), choices=marca_options)
    ejecutivo = models.ForeignKey(Perfil, on_delete=models.CASCADE, verbose_name=_('Usuario'))

    #tag = models.ManyToManyField(Tag, blank=True,)
    def __str__(self):
        return '%s' % self.nombre
# Create your models here.



class EstadosJson(models.Model):
    """ Subir json estados desde admin """
    name = models.CharField(max_length=255)
    file = models.FileField(upload_to="static/files")

    def __unicode__(self):
        return self.name


class MunicipiosJson(models.Model):
    """ Subir json municipios desde admin """
    name = models.CharField(max_length=255)
    file = models.FileField(upload_to="static/files")

    def __unicode__(self):
        return self.name


@receiver(post_save, sender=EstadosJson)
def estadojson(sender, instance, **kwargs):
    """ Senal crear estados """
    urlj = BASE_DIR + instance.file.url
    json_data = open(urlj)
    data = json.load(json_data)
    for val in data:
        p, created = Estado.objects.get_or_create(idestado=val['idestado'], estado=val['estado'])
        if created:
            p.save()
    json_data.close()


@receiver(post_save, sender=MunicipiosJson)
def municipiojson(sender, instance, **kwargs):
    """ Senal crear municipios """
    urlj = BASE_DIR + instance.file.url
    json_data = open(urlj)
    data = json.load(json_data)
    for val in data:
        estado = get_object_or_404(Estado, idestado=val['idestado'])
        p, created = Municipio.objects.get_or_create(estado=estado, idmunicipio=val['idm'], municipio=val['municipio'])
        if created:
            p.save()
    json_data.close()


