from django.contrib import admin
from clientes.models import Client, Estado, Municipio, EstadosJson, MunicipiosJson

# Register your models here.
@admin.register(Client)
class PerfilAdmin(admin.ModelAdmin):
    list_display = ('nombre',)
    list_per_page = 30



@admin.register(Estado)
class EstadoAdmin(admin.ModelAdmin):
    model = Estado
    list_display = ('estado',)


@admin.register(Municipio)
class MunicipioAdmin(admin.ModelAdmin):
    model = Municipio
    list_display = ('estado', 'municipio',)





@admin.register(EstadosJson)
class EstadosJsonAdmin(admin.ModelAdmin):
    model = EstadosJson


@admin.register(MunicipiosJson)
class MunicipiosJsonAdmin(admin.ModelAdmin):
    model = MunicipiosJson
