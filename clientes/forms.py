from django import forms
from clientes.models import Client, Estado, Municipio
from django.utils.translation import ugettext as _

class ClientForm(forms.ModelForm):

    empresa = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Empresa(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Empresa')},
        label=_('Empresa(s)*'),
        required=True,
    )
    
    email_Corporativo = forms.EmailField(
        widget=forms.TextInput(attrs={'placeholder': _('Email Corporativo'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese su email')},
        label=_('Email Corporativo'),
        required=True,
    )



    nombre = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Nombre(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Nombre')},
        label=_('Nombre(s)'),
        required=True,
    )

    apellido = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Apellido(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Apellido')},
        label=_('Apellido(s)'),
        required=True,
    )


    email_Personal = forms.EmailField(
        widget=forms.TextInput(attrs={'placeholder': _('Email Personal'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese su email')},
        label=_('Email Personal'),
        required=True,
    )

    telefono = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Teléfono'), 'class': "form-control"}),
        error_messages={'required': _(u'Teléfono')},
        label=_('Teléfono'),
        required=True,
    )

    celular = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Celular'), 'class': "form-control"}),
        error_messages={'required': _(u'Celular')},
        label=_('Celular'),
        required=True,
    )

    date = forms.DateField(
        widget=forms.DateInput(attrs={'placeholder': _('Cumpleaños'), 'class': "form-control"}),
        label=_('Cumpleaños'),
    )




    nombre2 = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Nombre(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Nombre')},
        label=_('Nombre(s)'),
        required=False,
    )

    apellido2 = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Apellido(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Apellido')},
        label=_('Apellido(s)'),
        required=False,
    )


    email_Personal2 = forms.EmailField(
        widget=forms.TextInput(attrs={'placeholder': _('Email Personal'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese su email')},
        label=_('Email Personal'),
        required=False,
    )

    telefono2 = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Teléfono'), 'class': "form-control"}),
        error_messages={'required': _(u'Teléfono')},
        label=_('Teléfono'),
        required=False,
    )

    celular2 = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Celular'), 'class': "form-control"}),
        error_messages={'required': _(u'Celular')},
        label=_('Celular'),
        required=False,
    )

    date2 = forms.DateField(
        widget=forms.DateInput(attrs={'placeholder': _('Cumpleaños'), 'class': "form-control"}),
        label=_('Cumpleaños'),
        required=False,
    )


    pais_options = (
        (1, 'México'),
        (2, 'Estados Unidos'),
        (3, 'Colombia'),
        (4, 'Peru'),
        (5, 'Chile'),
        (5, 'España'),
        (5, 'Argentina'),
    )

    pais = forms.ChoiceField(widget=forms.Select(attrs={'placeholder': _('País'), 'class': "form-control"}), choices=pais_options,required=False,)

    direccion_empresa = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Dirección Empresa'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Dirección de Empresa')},
        label=_('Dirección de Empresa*'),
        required=True,
    )



    estado = forms.ModelChoiceField(
        queryset=Estado.objects.all(),
        widget=forms.Select( attrs={'class': "form-control"}),
    )

    municipio = forms.ModelChoiceField(
        queryset=Municipio.objects.all(), 
        widget=forms.Select( attrs={'class': "form-control"}),
    )

    comentarios = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder': _('Comentarios'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Load')},
        label=_('Comentarios'),
        required=True,
    )

    

    dominio = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Sitio Web'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Load')},
        label=_('Sitio Web'),
        required=True,
    )


    rfc = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('RFC'), 'class': "form-control"}),
        max_length=80,
        label=_('RFC'),
        required=False,
    )

    razonsocial = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Razón social'), 'class': "form-control"}),
        max_length=80,
        label=_('Razón social'),
        required=False,
    )
    direccion = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Dirección'), 'class': "form-control"}),
        max_length=80,
        label=_('Dirección'),
        required=False,
    )

    correo = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Correo'), 'class': "form-control"}),
        max_length=80,
        label=_('Correo'),
        required=False,
    )

    gasto_options = (
        (1, '[D01] - Honorarios médicos, dentales y gastos hospitalarios.'),
        (2, '[D02] - Gastos médicos por incapacidad o discapacidad'),
        (3, '[D03] - Gastos funerales.'),
        (4, '[D04] - Donativos.'),
        (5, '[D05] - Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).'),
        (6, '[D06] - Aportaciones voluntarias al SAR.'),
        (7, '[D07] - Primas por seguros de gastos médicos.'),
        (8, '[D08] - Gastos de transportación escolar obligatoria.'),
        (9, '[D09] - Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.'),
        (10, '[D10] - Pagos por servicios educativos (colegiaturas)'),
        (11, '[G01] - Adquisición de mercancias'),
        (12, '[G02] - Devoluciones, descuentos o bonificaciones'),
        (13, '[G03] - Gastos en general'),
        (14, '[I01] - Construcciones'),
        (15, '[I02] - Mobilario y equipo de oficina por inversiones'),
        (16, '[I03] - Equipo de transporte'),
        (17, '[I04] - Equipo de computo y accesorios'),
        (18, '[I05] - Dados, troqueles, moldes, matrices y herramental'),
        (19, '[I06] - Comunicaciones telefónicas'),
        (20, '[I07] - Comunicaciones satelitales'),
        (21, '[I08] - Otra maquinaria y equipo'),
        (22, '[P01] - Por definir')
    )

    uso_cfdi = forms.ChoiceField(widget=forms.Select(attrs={'placeholder': _('Uso CFDI'), 'class': "form-control"}), choices=gasto_options,required=False,)

    comentario_factura = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder': _('Comentario Factura'), 'class': "form-control"}),
        max_length=80,
        label=_('Comentario Factura'),
        required=False,
    )


    rfc_2 = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('RFC'), 'class': "form-control"}),
        max_length=80,
        label=_('RFC'),
        required=False,
    )

    razonsocial_2 = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Razón social'), 'class': "form-control"}),
        max_length=80,
        label=_('Razón social'),
        required=False,
    )
    direccion_2 = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Dirección'), 'class': "form-control"}),
        max_length=80,
        label=_('Dirección'),
        required=False,
    )

    correo_2 = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Correo'), 'class': "form-control"}),
        max_length=80,
        label=_('Correo'),
        required=False,
    )

    uso_cfdi_2 = forms.ChoiceField(widget=forms.Select(attrs={'placeholder': _('Uso CFDI'), 'class': "form-control"}), choices=gasto_options,required=False,)

    comentario_factura_2 = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder': _('Comentario Factura'), 'class': "form-control"}),
        max_length=80,
        label=_('Comentario Factura'),
        required=False,
    )


    origen_options = (
        (1, 'Google'),
        (2, 'Facebook'),
        (3, 'Yahoo'),
        (4, 'Recomendado'),
        (5, 'Otro'),
    )


    origen = forms.ChoiceField(widget=forms.Select(attrs={'placeholder': _('Origen'), 'class': "form-control"}), choices=origen_options,required=False,)

    marca_options = (
        (1, 'Novemp'),
        (2, 'New'),
        (3, 'Youp'),
        (4, 'Markon'),
    )

    marca = forms.ChoiceField(widget=forms.Select(attrs={'placeholder': _('Marca'), 'class': "form-control"}), choices=marca_options,required=False,)





    """tag = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Tag(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese Tag')},
        label=_('Tag(s)'),
        required=True,
    )"""



    class Meta:
        model = Client
        fields = ('empresa','email_Corporativo', 'nombre','apellido', 'telefono','celular','date','email_Personal', 'estado', 'municipio', 'comentarios', 'dominio', 'nombre2','apellido2', 'telefono2','celular2','date2','email_Personal2', 'rfc','razonsocial', 'direccion', 'correo', 'uso_cfdi','comentario_factura','origen', 'marca','ejecutivo','rfc_2','razonsocial_2', 'direccion_2', 'correo_2', 'uso_cfdi_2','comentario_factura_2', 'direccion_empresa', 'pais')

    


