from django.shortcuts import render, redirect
from clientes.forms import ClientForm
from clientes.models import Client, Estado, Municipio
from django.core.mail import send_mail
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

from django.utils.decorators import method_decorator

from django.views.generic import TemplateView, ListView

from proyectos.models import Proyect
from django.db.models import Q 


from django.http import HttpResponse, JsonResponse
import json

from django import forms
def add_client(request):

    if request.method == 'POST':
        formcli = ClientForm(request.POST) 
        if formcli.is_valid():
            formcli.save()

  
            return redirect('list_client')

    else:
        formcli = ClientForm(initial={'ejecutivo': request.user,}) #para crer un nuevo objeto
        formcli.fields['ejecutivo'].widget = forms.HiddenInput()

    return render(
        request=request,
        template_name='add_client.html',
        context={
            
            'formcli': formcli
        })

# guardar cliente-proyecto mediante ajax
@login_required(login_url='/login')
@csrf_exempt
def ajax_save_cliente(request, pk):
    data = request.body
    data = json.loads(data)

    nombre = (data['nombre'])
    email_Corporativo = (data['email_Corporativo'])
    email_Personal = (data['email_Personal'])  
    telefono = (data['telefono'])  
    empresa = (data['empresa'])  
    ciudad = (data['ciudad'])     
    comentarios = (data['comentarios'])
    date = (data['date'])
    dominio = (data['dominio'])
    tag = (data['tag'])



    cliente = Client.objects.get(pk=pk)#sera la relacion del pk de tarea 
    client = Client.objects.create(nombre=nombre, tag=tag, dominio=dominio, date=date, comentarios=comentarios, telefono=telefono, empresa=empresa, ciudad=ciudad, email_Corporativo=email_Corporativo, proyecto=proyecto, email_Personal=email_Personal)
    response = JsonResponse({ 'success': 1, 'tag': client.tag, 'nombre': client.nombre, 'ciudad': client.ciudad, 'email_Personal': client.email_Personal,'email_Corporativo': client.email_Corporativo,  'empresa': client.empresa, 'telefono': client.telefono, 'comentarios': client.comentarios, 'dominio': client.dominio, 'proyecto': client.proyecto.pk, 'date': client.date,})
    return response 



def list_client(request):

    clientes = Client.objects.all()

    return render(
        request=request,
        template_name='list_client.html',
        context={
             'clientes': clientes,
          
        })

# listar cliente proyecto mediante ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_list_cliente(request, pk):
    cliente = Client.objects.get(pk=pk)#sera la relacion del pk de tarea
    clientes = Client.objects.filter(proyecto = proyecto)

    print('entro vista ajax')
    listclient =[]
    for  cliente in  clientes:
        #crear un diccionario con los datos de comentariotarea, 
        clientdic = {
            'nombre': cliente.nombre, 
            'email_Corporativo': cliente.email_Corporativo,
            'email_Personal': cliente.email_Personal,
            'telefono': cliente.telefono,
            'empresa': cliente.empresa,
            'ciudad': cliente.ciudad,
            'comentarios': cliente.comentarios,
            'date': str(cliente.comentarios), 
            'dominio': cliente.dominio, 
            'tag': cliente.tag,
            }

        #metodo append de agregar diccionario
        listclient.append(clientdic)


    response = json.dumps(listclient,  ensure_ascii=False)
    print(response)
    return JsonResponse(response, safe=False)



def edit_client(request, pk):
    #instancia
    cliente = Client.objects.get(pk=pk)
    # Creamos formulario con los datos de la instancia
    form = ClientForm(request.POST, instance=cliente)  #objeto
    #comprobamos formulario
    if request.method == 'POST':
        # Actualizamos el formulario con los datos recibidos
        form = ClientForm(request.POST, instance=cliente)
        # Si el formulario es válido
        if form.is_valid():
            cliente.user.delete()
            return redirect('home')
    else:
        form = ClientForm(instance=cliente) #nuevo objeto
       

    return render(
        request=request,
        template_name='edit_client.html',
        context={
            
            'form': form,
            'cliente': cliente,
        })

@login_required(login_url='/login')
def ajax_edit_cliente(request, pk):
    #instancia
    cliente = Client.objects.get(pk=pk)
    # Creamos formulario con los datos de la instancia
    formcli = ClientForm(request.POST,)  #objeto
    #comprobamos formulario
    if request.method == 'POST':
        # Actualizamos el formulario con los datos recibidos
        formcli = ClientForm(request.POST,)
        # Si el formulario es válido
        if formcli.is_valid():
            formcli.save()
            cliente = formser.save(commit=False)
            cliente.save()
            return redirect('home')
    else:
        formcli = ClientForm() #nuevo objeto
       

    return render(
        request=request,
        template_name='edit_servicio.html',
        context={
            
            'formcli': formcli,
            'cliente': cliente,
        })




def detail_client(request, pk):


   clientes = Client.objects.get(pk=pk)

   proyectos = Proyect.objects.filter(client = clientes )


   return render(
        request=request,
        template_name='detail_client.html',
        context={
             'clientes': clientes,
              'proyectos': proyectos,
          
        })

def delete_client(request, pk):
     #instancia
    cliente = Client.objects.get(pk=pk)
    if cliente:
        cliente.delete()
        return redirect('list_client')
    

# actualizar_servicio-proyecto ajax
@csrf_exempt
@login_required(login_url='/login')
def ajax_save_fill_client(request, pk):
    data = request.body
    data = json.loads(data)
    nombre = (data['nombre'])
    email_Corporativo = (data['email_Corporativo'])
    email_Personal = (data['email_Personal'])  
    telefono = (data['telefono'])  
    empresa = (data['empresa'])  
    ciudad = (data['ciudad'])     
    comentarios = (data['comentarios'])
    date = (data['date'])
    dominio = (data['dominio'])

    cliente = Client.objects.get(pk=pk)#sera la relacion del pk de tarea 
    Client.objects.filter(pk=cliente.pk).update(nombre=nombre, dominio=dominio, date=date, comentarios=comentarios, telefono=telefono, empresa=empresa, ciudad=ciudad, email_Corporativo=email_Corporativo, email_Personal=email_Personal)
    response = JsonResponse({ 'success': 1,})

    return response

# rellenar forumulario
@csrf_exempt
@login_required(login_url='/login')
def ajax_fill_client(request, pk):
    client = Client.objects.get(pk=pk)
    response = JsonResponse({ 'success': 1, 'nombre': client.nombre, 'tag': client.tag, 'ciudad': client.ciudad, 'email_Personal': client.email_Personal,'email_Corporativo': client.email_Corporativo,  'empresa': client.empresa, 'telefono': client.telefono, 'comentarios': client.comentarios, 'dominio': client.dominio, 'proyecto': client.proyecto.pk, 'date': client.date,})

    return response    
        
    



@method_decorator(login_required, name='dispatch')
#@method_decorator(group_required('Administrador'), name='dispatch')
class search_clients(ListView): 
    model = Client
    template_name = 'search.html'
    

# funcion buscador de proyectos 
    def get_queryset(self):
        nombre = self.request.GET.get('data', None)
        print('query', nombre)
        try:
            if nombre != '':
                print('entro try')
                listpro = Client.objects.filter(Q(nombre__contains=nombre)|Q(empresa__contains=nombre)|Q(email_Personal__contains=nombre)|Q(email_Corporativo__contains=nombre),)
        except:
            print('except')
            listpro = Client.objects.all()

        print('listpro, ', listpro)
     

        return listpro




@login_required
def filtros_estados(request):
    """ Obtener estados json """
    estados = Estado.objects.all()
    data = {t.pk: {'pk': t.pk, 'estado': t.estado} for t in estados}
    return JsonResponse(data, safe=False)

@login_required
def filtros_municipios(request, pk):
    """ Obtener municipios en base a estado json """
    municipios = Municipio.objects.filter(estado=pk)
    data = {t.pk: {'pk': t.pk, 'municipio': t.municipio} for t in municipios}
    return JsonResponse(data, safe=False)





